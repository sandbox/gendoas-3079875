<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('DOKU_INC')) die();

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
/**
 *
 * Description of JuiDataIO
 *
 * @author Albin
 */
class JuiDataIO {
    private static $_plugin         = false;
    private static $_juidatadir     = false;
    private static $_user           = false;
    private static $configloaded    = false;
    private static $conf            = array();

    // this implements the 'singleton' design pattern.
    static function &getInstance () {
        static $instance;
        if (!isset($instance)) {
            $instance = new JuiDataIO;
        }
        return $instance;
    }

    private function __construct() {
        global $conf;
        $this->_plugin = 'juiwidget';
        // prepare jui data directory
        $juidatadir = DOKU_INC.$conf['savedir'].'/'.$this->_getConf('juidatadir');
        if (!@file_exists($juidatadir)) io_mkdir_p($juidatadir);
        $conf['juidatadir'] = init_path($juidatadir);
        $this->_juidatadir = $conf['juidatadir'];
    }

    public function storeData($target, $key, $data) {
        if ($data == $this->retrieveData($target, $key)) {
            return true;    // nothing to do
        }
        return io_saveFile($this->_getFileName($target, $key), $data);
    }
    public function retrieveData($target, $key) {
        return io_readFile($this->_getFileName($target, $key));
    }
    private function _getFileName($target, $key, $ext='.json') {
        global $conf, $USERINFO;
        static $files = array();

        $client = $_SERVER['REMOTE_USER'];
        if(!$client) {
            $client = clientIP(true);
            $target .= 'tmp';
        }
        $fkey = "$target/$key/$client";
        if (isset($files[$fkey])) {
            return $files[$fkey];
        }

        $md5  = md5($fkey);
        $file = $conf['juidatadir'].'/'.$target.'/'.$md5[0].'/'.$md5[1].'/'.$md5[2].'/'.$md5.$ext;
        $files[$fkey] = $file;
        return $file;
    }


    /**
     * _getConf($setting) -(copy from plugin.php)
     *
     * use this function to access plugin configuration variables
     */
    private function _getConf($setting){

        if (!$this->configloaded){ $this->_loadConfig(); }

        return $this->conf[$setting];
    }

    /**
     * _loadConfig() - copy from plugin.php
     * merges the plugin's default settings with any local settings
     * this function is automatically called through _getConf()
     */
    private function _loadConfig(){
        global $conf;

        $defaults = $this->_readDefaultSettings();
        $plugin = $this->_plugin;

        foreach ($defaults as $key => $value) {
            if (isset($conf['plugin'][$plugin][$key])) continue;
            $conf['plugin'][$plugin][$key] = $value;
        }

        $this->configloaded = true;
        $this->conf =& $conf['plugin'][$plugin];
    }

    /**
     * (copy from plugin.php)
     * read the plugin's default configuration settings from conf/default.php
     * this function is automatically called through _getConf()
     *
     * @return    array    setting => value
     */
    private function _readDefaultSettings() {

        $path = DOKU_PLUGIN.$this->_plugin.'/conf/';
        $conf = array();

        if (@file_exists($path.'default.php')) {
            include($path.'default.php');
        }

        return $conf;
    }
}
?>
