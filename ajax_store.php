<?php
/**
 * AJAX ...
 *
 * @license     GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */

ignore_user_abort(true);    // prevent abort storage
 
//fix for Opera XMLHttpRequests
if ( !isset($HTTP_RAW_POST_DATA) ) {
    $HTTP_RAW_POST_DATA = file_get_contents("php://input");
}
if(!count($_POST) && $HTTP_RAW_POST_DATA){
  parse_str($HTTP_RAW_POST_DATA, $_POST);
}

if (!defined('DOKU_INC')) define('DOKU_INC',dirname(__FILE__).'/../../../');
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');

require_once(DOKU_INC.'inc/init.php');
require_once(DOKU_PLUGIN_JUI.'juidata_io.php');

//close session
session_write_close();

$target = $_REQUEST['target'];
$key    = $_REQUEST['key'];
$data   = $_REQUEST['juidata'];

$juiDataIO = JuiDataIO::getInstance();
$result = $juiDataIO->storeData($target, $key, $data);

// ------------- end -------------