<?php
/**
 * TODO:: for DokuWiki Plugin juiwidget
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');

require_once DOKU_PLUGIN_JUI.'juiutils.php';

class JuiSchemes {

    private static $_juiSchemes = array();
    private static $_juiSchemeFNs = array();
    private static $_juiBaseFNs = array();

    // this implements the 'singleton' design pattern.
    static function &getInstance () {
        static $instance;
        if (!isset($instance)) {
            $instance = new JuiSchemes;
        }
        return $instance;
    }

    // prevent creating new instance
    private function __construct() {
    }

    static function getMetaFN($plugin, $id, $fullpath=true) {
        $path = $fullpath ? DOKU_PLUGIN : '';
        return $path.$plugin."/schemes/meta.$id.json";
    }

    static function getMetaData($plugin, $id) {
        static $metaData = array();
        $key = "$plugin-$id";
        if ( isset($metaData[$key]) ) {
            return $metaData[$key];
        }

        $file = self::getMetaFN($plugin, $id);;
        $ret = @json_decode(@file_get_contents($file), true);
        if (empty($ret)) {
            $file = juiPathPluginRelative($file);
            juiDbgMsg("Error loading metadata (json-syntax?): $file");
            return false;
        }
        $metaData[$key] = $ret;
        return $ret;
    }

    static function schemeDataClean($sd) {
        // clean add/remove entries
        foreach ($sd as $key => $data) {
            foreach (array('Class', 'Flags') as $item) {
                $add = $data["add$item"];
                $rem = $data["remove$item"];
                if (!is_string($add) || !is_string($rem)) continue;

                $add = array_map( "trim", explode(' ', $add) );
                $rem = array_map( "trim", explode(' ', $rem) );
                if (empty($add) || empty($rem)) continue;

                $isec = array_intersect($add, $rem);
                if (empty($isec)) continue;

                $sd[$key]["add$item"] = implode(' ', array_diff($add, $isec));
                $sd[$key]["remove$item"] = implode(' ', array_diff($rem, $isec));
                if (empty($sd[$key]["add$item"])) unset($sd[$key]["add$item"]);
                if (empty($sd[$key]["remove$item"])) unset($sd[$key]["remove$item"]);
            }
        }

        return $sd;
    }


    function getSchemeInstance($plugin, $id='default') {
        if ( empty($id) ) $id='default';
        if ( !isset($this->_juiSchemes[$plugin][$id]) ) {
            // cache the scheme-array !!!
//                    $baseFN = '';
            $this->_juiSchemes[$plugin][$id] = $this->_getSchemeDataFinal($plugin, $id, $baseFN);
            $this->_juiSchemeFNs[$plugin][$id] = $this->_getSchemeFN($plugin, $id);
            $this->_juiBaseFNs[$plugin][$id] = $baseFN;
        }
        // return new instance !!!
        $instance = new JuiScheme($this->_juiSchemes[$plugin][$id]);
        $instance->setDataAllowed(self::getMetaData($plugin, 'optsallowed'));
        $instance->setSchemeName("$plugin-$id");
        $instance->setFileName($this->_juiSchemeFNs[$plugin][$id]);
        $instance->setBaseFileName($this->_juiBaseFNs[$plugin][$id]);
        return $instance;
    }

    private function _getSchemeDataFinal($plugin, $id, &$baseFN) {
        $baseFN = '';
        $sd = $this->_getSchemeData($plugin, $id);
        if (is_array($sd) && isset($sd['base'])) {
            $base = self::getMetaData($plugin, $sd['base']);
            $baseFN = self::getMetaFN($plugin, $sd['base']);
            unset($sd['base']);
            $sd = juiArrayMergeRecursiveSimple($base, $sd);
            $sd = self::schemeDataClean($sd);
        }
        return $sd;
    }
    private function _getSchemeData($plugin, $id) {
        $thisJSON = $this->_readSchemeJSON($plugin, $id);
        return json_decode($thisJSON, true);
    }


    private function _readSchemeJSON($plugin, $id) {
        $file = $this->_getSchemeFN($plugin, $id);
        if( @file_exists($file) ) {
            return file_get_contents($file);
        }
        return '';
    }
    private function _getSchemeFN($plugin, $id) {
        $file = DOKU_PLUGIN.$plugin.'/schemes/'.$id.'.json';
        if( !@file_exists($file) ){
            //fall back to default
            $file = DOKU_PLUGIN.$plugin.'/schemes/default.json';
        }
        return $file;
    }


}

class JuiScheme {
    private $_schemeData = array();
    private $_dataAllowed = array();
    private $_schemeName = false;
    private $_fileName = false;
    private $_baseFileName = false;

    function __construct($schemeData) {
        $this->_schemeData = $schemeData;
    }

    function setDataAllowed($allowed) {
        $this->_dataAllowed = $allowed;
    }

    function getSchemeName() {
        return $this->_schemeName;
    }
    function setSchemeName($schemename) {
        $this->_schemeName = $schemename;
    }

    function getFileName() {
        return $this->_fileName;
    }
    function setFileName($filename) {
        $this->_fileName = $filename;
    }

    function getBaseFileName() {
        return $this->_baseFileName;
    }
    function setBaseFileName($filename) {
        $this->_baseFileName = $filename;
    }

    function hasData() {
        return count($this->_schemeData) > 0;
    }
    function getSchemeData() {
        return $this->_schemeData;
    }

    function getVarsJS($data, $plugin) {
        $ret = array();

        // main options first,
        if ( !isset($data['opts'][$plugin]) ) {
            juiDbgMsg("Error plugin-options ($plugin)");
            return array();
        }

        foreach ($data['opts'] as $key => $opts) {
            $varsjs = $this->_getVarsJsItem($key, $opts);
            if ($plugin == 'juilayout' && $key == $plugin) {
                $this->_addLayoutPanes($varsjs, $plugin);
            }
            if (!empty($varsjs)) $ret[$key] = $varsjs;
        }
        
        if (isset($ret[$plugin])) {
            $rp =& $ret[$plugin];
            $rs = isset($rp['addFlags']) && isset($rp['addFlags']['resizable']) ? true : false;
            if (!$rs && isset($ret['resizer']) ) unset($ret['resizer']);
            $fs = isset($rp['addFlags']) && isset($rp['addFlags']['fillspace']) ? true : false;
            if ($fs && isset($rp['style']) ) {
                $style =& $rp['style'];
                if (isset($style['height'])) unset($style['height']);
                if (isset($style['width'])) unset($style['width']);
                if (empty($style)) unset($rp['style']);
            }
            $cs = isset($rp['addFlags']) && isset($rp['addFlags']['collapsible']) ? true : false;
            if ($cs) {
                $rp['opts']['collapsible'] = 1;
                unset($rp['addFlags']['collapsible']);
                if (empty($rp['addFlags'])) unset($rp['addFlags']);
            }
        }
        

        // now sections, if exists
        $sects = $data['sections'];
        if ( isset($sects) && is_array($sects) ) {
            foreach ($sects as $idx => $secdata) {
                if ( !isset($secdata['opts']) ) continue;
                foreach ($secdata['opts'] as $key => $opts) {
                    if (empty($opts)) continue;
                    $varsjs = $this->_getVarsJsItem($key, $opts, $idx);
                    if (!empty($varsjs)) $ret['sections'][$idx][$key] = $varsjs;
                }
            }
        }

        return $ret;
    }

    private function _getVarsJsItem($optskey, $opts, $sidx=false) {
        $ret = array();
                    // no!! do not return !!!  
                    // we need the schema-opts !!!
                    // if (empty($opts)) return $ret;

        // prepare Class
        $this->_parseClassCSS($optskey, $opts, $sidx);
        $this->_parseFlags($optskey, $opts, $sidx);
        $this->_parseOptsJUI($optskey, $opts, $sidx);
        $this->_parseStyleCSS($optskey, $opts, $sidx);
        $this->_parseAttr($optskey, $opts, $sidx);

        // get vars
        // Class (add, remove)
        $ret['addClass'] = $this->_getClassCSS($optskey, 'add', $sidx);
        if (empty($ret['addClass'])) unset($ret['addClass']);
        $ret['removeClass'] = $this->_getClassCSS($optskey, 'remove', $sidx);
        if (empty($ret['removeClass'])) unset($ret['removeClass']);
        // Flags (add only, remove not needed)
        $ret['addFlags'] = $this->_getFlags($optskey, 'add', $sidx);
        if (empty($ret['addFlags'])) unset($ret['addFlags']);


        // opts
        $ret['opts'] = $this->_getOptsJUI($optskey, $sidx);
        if (empty($ret['opts'])) unset($ret['opts']);
        // style
        $ret['style'] = $this->_getStyleCSS($optskey, $sidx);
        if (empty($ret['style'])) unset($ret['style']);
        // attr
        $ret['attr'] = $this->_getAttr($optskey, $sidx);
        if (empty($ret['attr'])) unset($ret['attr']);


        return $ret;
    }

    private function _parseClassCSS($id, $opts, $sidx=false) {
        // add, remove, diff
        if ( isset($opts['addClass']) ) {
            $this->_addClassCSS($id, 'add', $opts['addClass'], $sidx);
        }
        if ( isset($opts['removeClass']) ) {
            $this->_addClassCSS($id, 'remove', $opts['removeClass'], $sidx);
        }
        $add = explode(' ', $this->_getClassCSS($id, 'add', $sidx));
        $remove = explode(' ', $this->_getClassCSS($id, 'remove', $sidx));
        if ( !empty($add) && !empty($remove) ) {
            $this->_setClassCSS($id, 'add', implode(' ', array_diff($add, $remove)), $sidx);
            $this->_setClassCSS($id, 'remove', implode(' ', array_diff($remove, $add)), $sidx);
        }
    }

    private function _parseFlags($id, $opts, $sidx=false) {
        // add, remove, diff
        $p = '/[\s,;]+/';
        if ( isset($opts['addFlags']) ) {
            $val = preg_split($p, $opts['addFlags'], -1, PREG_SPLIT_NO_EMPTY);
            $val = array_fill_keys($val, 1);
            $this->_addFlags($id, 'add', $val, $sidx);
        }
        if ( isset($opts['removeFlags']) ) {
            $val = preg_split($p, $opts['removeFlags'], -1, PREG_SPLIT_NO_EMPTY);
            $val = array_fill_keys($val, 1);
            $this->_addFlags($id, 'remove', $val, $sidx);
        }
        $add = $this->_getFlags($id, 'add', $sidx);
        $remove = $this->_getFlags($id, 'remove', $sidx);
        if ( !empty($add) && !empty($remove) ) {
            $this->_setFlags($id, 'add', array_diff_assoc ($add, $remove), $sidx);
            $this->_setFlags($id, 'remove', array_diff_assoc ($remove, $add), $sidx);
        }
    }

    private function _parseOptsJUI($id, $opts, $sidx=false) {
        // add only
        if ( isset($opts['opts']) ) {
            $this->_addOptsJUI($id, $opts['opts'], $sidx);
        }
    }

    private function _parseStyleCSS($id, $opts, $sidx=false) {
        // add only
        if ( isset($opts['style']) ) {
            $this->_addStyleCSS($id, $opts['style'], $sidx);
        }
    }

    private function _parseAttr($id, $opts, $sidx=false) {
        // add only
        if ( isset($opts['attr']) ) {
            $this->_addAttr($id, $opts['attr'], $sidx);
        }
    }

    // ClassCSS (_get, _set, _add)
    private function _getClassCSS($id, $action, $sidx=false) {
        return $this->_getValueString($id, "{$action}Class", $sidx);
    }
    private function _setClassCSS($id, $action, $value, $sidx=false) {
        $this->_setValueString($id, "{$action}Class", $value, $sidx);
    }
    private function _addClassCSS($id, $action, $add, $sidx=false) {
        $this->_addValueString($id, "{$action}Class", $add, $sidx);
    }

    // Flags (_get, _set, _add)
    private function _getFlags($id, $action, $sidx=false) {
        return $this->_getValueArray($id, "{$action}Flags", $sidx);
    }
    private function _setFlags($id, $action, $value, $sidx=false) {
        $this->_setValueArray($id, "{$action}Flags", $value, $sidx);
    }
    private function _addFlags($id, $action, $add, $sidx=false) {
        $this->_addValueArray($id, "{$action}Flags", $add, $sidx);
    }

    // OptsJUI (_get, _set, _add)
    private function _getOptsJUI($id, $sidx=false) {
        return $this->_getValueArray($id, 'opts', $sidx);
    }
    private function _setOptsJUI($id, $value, $sidx=false) {
        $this->_setValueArray($id, 'opts', $value, $sidx);
    }
    private function _addOptsJUI($id, $add, $sidx=false) {
        $this->_addValueArray($id, 'opts', $add, $sidx);
    }

    // StyleCSS (_get, _set, _add)
    private function _getStyleCSS($id, $sidx=false) {
        return $this->_getValueArray($id, 'style', $sidx);
    }
    private function _setStyleCSS($id, $value, $sidx=false) {
        $this->_setValueArray($id, 'style', $value, $sidx);
    }
    private function _addStyleCSS($id, $add, $sidx=false) {
        $this->_addValueArray($id, 'style', $add, $sidx);
    }

    // Attr (_get, _set, _add)
    private function _getAttr($id, $sidx=false) {
        return $this->_getValueArray($id, 'attr', $sidx);
    }
    private function _setAttr($id, $value, $sidx=false) {
        $this->_setValueArray($id, 'attr', $value, $sidx);
    }
    private function _addAttr($id, $add, $sidx=false) {
        $this->_addValueArray($id, 'attr', $add, $sidx);
    }

    // ValueString (_get, _set, _add)
    private function _getValueString($id, $sid, $sidx=false) {
        $target =& $this->_getTargetVar($sidx);
        if ( isset($target[$id][$sid])
        && is_string($target[$id][$sid]) ) {
            return $target[$id][$sid];
        }
        return '';
    }
    private function _setValueString($id, $sid, $value, $sidx=false) {
        $target =& $this->_getTargetVar($sidx);
        $target[$id][$sid] = $value;
    }
    private function _addValueString($id, $sid, $add, $sidx=false) {
        $add = @trim($add);
        if (empty($add) || !is_string($add)) {
            return;     // nothing to do
        }

        $target =& $this->_getTargetVar($sidx);
        $cur = $this->_getValueString($id, $sid, $sidx);
        if ( empty($cur) ) {
            $target[$id][$sid] = $add;
        } else {
            $cur = array_map( "trim", explode(' ', $cur) );
            $add = array_map( "trim", explode(' ', $add) );
            $cur = array_merge($cur, array_diff($add, $cur));
            $target[$id][$sid] = implode(' ', $cur);
        }
    }

    // ValueArray (_get, _set, _add)
    private function _getValueArray($id, $sid, $sidx=false) {
        $target =& $this->_getTargetVar($sidx);
        if ( isset($target[$id][$sid])
        && is_array($target[$id][$sid]) ) {
            return $target[$id][$sid];
        }
        return array();
    }
    private function _setValueArray($id, $sid, $value, $sidx=false) {
        $target =& $this->_getTargetVar($sidx);
        $target[$id][$sid] = $value;
    }
    private function _addValueArray($id, $sid, $add, $sidx=false) {
        if (empty($add) || !is_array($add)) {
            return;     // nothing to do
        }

        $target =& $this->_getTargetVar($sidx);
        $cur = $this->_getValueArray($id, $sid, $sidx);
        if ( empty($cur) ) {
            $target[$id][$sid] = $add;
        } else {
            $cur = array_merge(array_diff_key($cur, $add), $add);
            $target[$id][$sid] = $cur;
        }
    }
    private function &_getTargetVar($sidx) {
        if ($sidx>0) {
            if (!isset($this->_schemeData['sects'][$sidx])) {
                $this->_schemeData['sects'][$sidx] = false;
            }
            return $this->_schemeData['sects'][$sidx];
        } else {
            return $this->_schemeData;
        }
    }
//                $this->_addLayoutPanes($varsjs, $plugin);
    private function _addLayoutPanes(&$varsjs, $plugin) {
        $sdata =& $this->_schemeData[$plugin];
        if (!isset($sdata)) return;
        
        $pvars = array();
        $panes = array('center', 'north', 'south', 'west', 'east');
        foreach ($panes as $pane) {
            if (empty($sdata[$pane])) continue;
            $pvars[$pane] = $sdata[$pane];
        }
        if (!empty($pvars)) $varsjs['panes'] = $pvars;
    }
}
?>
