<?php
/**
 * DokuWiki Plugin juiwidget (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin.spreizer@web.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_LF')) define('DOKU_LF', "\n");
if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');

if (!defined('CLS_JUICONTENT')) define('CLS_JUICONTENT','juicontent ');

require_once DOKU_PLUGIN.'syntax.php';
require_once DOKU_PLUGIN_JUI.'juicounter.php';
require_once DOKU_PLUGIN_JUI.'juischemes.php';


class syntax_plugin_juiwidget extends DokuWiki_Syntax_Plugin {

    // required overwrite
    function getType() { return 'substition';}
    function getPType() { return 'block';}
    function getSort() { return 300; }

    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<juisyscall.+?>',$mode,'plugin_juiwidget');
    }
    
    public function handle($match, $state, $pos, $handler){
        $data = array('match' => $match);
        list($data['mode'], $opts) = explode(' ', substr($match, 1, -1), 2);
        $data['opts'] = juiGetOptionsArray($opts, '*');
        return $data;
    }
    
    public function render($format, $renderer, $data) {
        if ($format != 'xhtml') return;
        if ($data['mode'] != 'juisyscall') return;  // the only now
        
        $call = isset($data['opts']['call']) ? $data['opts']['call'] : 'none';
        require_once DOKU_PLUGIN_JUI . 'juisyscall.php';
        if (class_exists('juisyscall')) {
            $param   = isset($data['opts'][$call]) ? $data['opts'][$call] : 'none';
            $syscall = new juisyscall();
            if ($syscall->parse($call, $param)) {
                $renderer->doc .= $syscall->getHtmlOutput();
            } else { $renderer->doc .= $syscall->getHtmlError(); }
            return true;
        }
//        switch($call){
//            case 'page':
//                if (class_exists('juisyscall')) {
//                    $page = isset($data['opts']['page']) ? $data['opts']['page'] : 'none';
//                    $syspage = new juisyspage();
//                    if ($syspage->parse($page)) {
//                        $renderer->doc .= $syspage->getHtmlOutput();
//                    }
//                    return true;
//                }
//                break;
//            default:
//                msg("Failed to handle syscall: " . $call, -1);
//                
//        }

        
        $xxx = 0;
        
    }
    

    /**
     * TODO
     *
     * @param string $uid
     * @param string $text if $uid not set create it ($uid=md5($text)
     * @param string $pre prefix (default='')
     * @return string expanded $uid
     */
    function getWidgetUID($uid, $text, $pre='') {
        if ( !isset($uid) ) {
            $uid = md5($text);
        }
        return $pre . JUI_UID_DELIMITER . $uid;
    }
    function getWidgetClasses($scheme) {
        $plugin = $this->getPluginName();
        return "juiwidget $plugin-widget $plugin-$scheme";
    }

    function getExtUID($uid, $idx='', $sec='') { return "$uid-$idx$sec"; }

    function getSchemeInstance($id='default') {
        $schemes =& JuiSchemes::getInstance();
        return $schemes->getSchemeInstance($this->getPluginName(), $id);
    }

    public function isPreview() {
        return juiGetJuiInfo()->isPreview();
    }
    public function isSyspage() {
        return juiGetJuiInfo()->isSyspage();
    }

    function extractOptions($content, &$data_opts) {
        $ret = $content;

        $offset = 0;
        $pos = strpos($content, '<opts_');
        while ($pos !== false) {
            $foo = ($pos > $offset) ? substr($content, $offset, $pos - $offset) : '';
            $foo = trim($foo);
            if ( !empty($foo) ) break;

            $pos = strpos($content, '>', $pos + 1);
            if ($pos !== false) {
                $offset = $pos + 1;
                $pos = strpos($content, '<opts_', $offset);
            }
        }

        // there are no options
        if (!$offset) {
            return $ret;
        }

        // $offset > 0:: extract options
        $opts = substr($content, 0, $offset);
        $ret = substr($content, $offset);

        // make an array of options
        $pattern = '&<opts_.+?>&is';
        preg_match_all($pattern, $opts, $options);
        foreach($options[0] as $opts){
            list($key, $value) = explode(' ', substr($opts, 6, -1), 2);
            $key = trim($key);
            if ( !empty($key) ) {
                $value = trim($value, '/');
                $data_opts[$key] = trim($value);
            }
        }

        return $ret;
    }

    function parseOptionString($opts, $keysallowed, $allowed_more=false) {
        if ( !isset($opts) || !is_array($opts) ) return false;

        $plugin = $this->getPluginName();
        // metadata for allowed options
        $opts_allowed = JuiSchemes::getMetaData($plugin, 'optsallowed');
        $keysallowed = $opts_allowed['keysallowed'][$keysallowed];
        if (is_string($allowed_more)) $keysallowed[] = $allowed_more;

        $result = array();
        foreach ($opts as $key => $value) {
            if ( !in_array($key, $keysallowed) ) continue;

            $allowed = $opts_allowed[$key];
            $allowed = (isset($allowed) && is_array($allowed)) ? $allowed : false;
            if ($allowed) {
                $result[$key] = juiGetOptionsArray($value, $allowed);
                if ( empty($result[$key]) ) unset($result[$key]);

                if ($key == $plugin && isset($result[$key]['schemeExt'])) {
                    $ext = $result[$key]['schemeExt'];
                    $af = $this->_addFlagsExt($result[$key]['addFlags'], $ext);
                    if (isset($af)) $result[$key]['addFlags'] = $af;
                    unset($result[$key]['schemeExt']);
                }
            }
        }
        foreach ($keysallowed as $key) {
            if (!isset($result[$key])) $result[$key] = array();
        }
        
        if ($this->isPreview() && isset($result[$plugin]) && !isset($result[$plugin]['uid'])) {
            // temp uid for preview required
            $result[$plugin]['uid'] = time() + JuiCounter::uidNext();
        }

        return $result;
    }
    
    private function _addFlagsExt($af, $ext) {
        $ext = str_split($ext);
        $ret = array();
        foreach ( $ext as $ix ) {
            switch (strtolower($ix)) {
                case 'b':   $ret[] = 'booktabs';    break;
                case 'f':   $ret[] = 'fillspace';   break;
                case 'r':   $ret[] = 'resizable';   break;
                case 'c':   $ret[] = 'collapsible'; break;
            }
        }
        if (empty($ret)) return $af;

        if (isset($af)) {
           $af = array_map('trim', explode(' ', $af));
           $ret = array_merge($ret, array_diff($af, $ret));
        }
        
        return implode(' ', $ret);
    }

    function htmlDivSellist($juiid, $sldata, $lm) {
        $itm_id  = $this->getExtUID($juiid, 'sellist');
        $html  = "\n" . $lm . '<div id="' . $itm_id . '" class="jui-sellist ui-helper-hidden">';
        // title and buttons
        $html .= "\n" . $lm . DOKU_TAB . '<div class="jui-sellist-title">'
                 . "\n" . $lm . DOKU_TAB . DOKU_TAB;
        // buttons: close, pin
        $html .= '<div class="jui-close-icon jui-icon-right ui-icon-circle-close">&nbsp;</div>'
                 . '<div class="jui-pin-icon jui-icon-right ui-icon-pin-w">&nbsp;</div>';
        $title = $sldata['title'] ? $sldata['title'] : $this->getLang('selection');
        $html .= '<div class="jui-floattoggle-icon jui-icon-right ui-icon-arrowthick-1-s">&nbsp;</div>'
                 . '<div>' . $title . '</div>' . "\n" . $lm . DOKU_TAB . '</div>';
        // list
        $html .= "\n" . $lm . DOKU_TAB . '<div class="jui-sellist-body"><ul>';
        $itxt = 'Lorem ipsum dol';
        for ($i = 1; $i < 2; $i++) {
            $html .= "\n" . $lm . DOKU_TAB . DOKU_TAB 
                     . '<li class="jui-sellist-li"><span class="jui-sellist-lispan">' 
                     . $itxt . '</span></li>';
        }
        $html .= "\n" . $lm . DOKU_TAB . '</ul></div>';

        // close sellist-div
        $html .= "\n" . $lm . '</div>';
        return $html;
    }
    function getIsNesting() {
        return juiGetIsNesting($this->getPluginName());
    }
    function setIsNesting($newValue) {
        return juiSetIsNesting($this->getPluginName(), $newValue);
    }

    function showMsgHtml($data) {
        $msg = $this->getLang($data['msgkey']);
        if ( empty($msg) ) {
            juiDbgMsg('LangMsg not found: ' . $data['msgkey']);
        }
        return $msg;
    }

    function headCaption($opts, $default) {
        if ( !isset($opts['head']['caption']) ) return $default;
        return $opts['head']['caption'];
    }

    function headTitle($opts, $default) {
        if ( !isset($opts['head']['title']) ) return $default;
        return $opts['head']['title'];
    }

    private function _cleanContent($content) {
        // prevent changing doublequotes for ajax by dokuwiki
        $pattern = '&<juiajax.+?>&is';
        preg_match_all($pattern, $content, $maches);
        if (!empty($maches[0])) {
            $replace = array();
            // dokuwiki ersetzt '"' eigenmächtig, deshalb maskieren
            foreach($maches[0] as $match){
                $replace[$match] = str_replace('"', '&quot;', $match);
            }
            $content = str_replace(array_keys($replace), array_values($replace), $content);
            juiAddAjaxReplace($replace);
        }


                        return $content;

                        
//        $ca = explode("\n",$content);
//        $cnt = count($ca);
//        if ( count($ca) > 0 ) {
//            $tmp = trim($ca[$cnt-1]);
//            if ( empty($tmp) ) { unset($ca[$cnt-1]); }
//            $tmp = trim($ca[0]);
//            if ( empty($tmp) ) { unset($ca[0]); }
//            $content = implode("\n",$ca);
//        }
//
//        return $content;
    }


                //JUIDEL::
    // MetaData müssen vorhanden sein
    function getMetaData($id) {
        static $metaData = array();
        if ( isset($metaData[$id]) ) {
            return $metaData[$id];
        }
        
        $plugin = $this->getPluginName();   // juiacc, juitabs, ...
        $file = JuiSchemes::getMetaFN($plugin, $id);;
        $ret = @json_decode(@file_get_contents($file), true);
        if (empty($ret)) {
            juiDbgMsg("Error loading metadata (json-syntax?): $plugin.$id");
            return false;
        }
        $metaData[$id] = $ret;
        return $ret;
    }

    function renderJuiContent($content, $juiid) {
        // clean and render content,
        // prevent nested widget (juiacc, juitabs, ...)
        // different widgets can be nested, but not identical
        $content = $this->_cleanContent($content);
        
        $juiInfo = juiGetJuiInfo();
                    $testVars = $this->_testVars();
                    
                    //JUIFIX::  chance section-id -> page-id zu verbinden
                    // weiterer parameter: $secid
                    
        $keepWidgetId = $juiInfo->setCurWidget($juiid);
        $keepIsNesting = $this->setIsNesting(true);
        $html = p_render('xhtml', p_get_instructions($content), $info);
        $this->setIsNesting($keepIsNesting);   // reset
        $juiInfo->setCurWidget($keepWidgetId);

        return $html;
    }
                //
                // Testfunctions
                //
                private function _testVars() {
                    global $ID;
                    $ret = array();

                    $ret['ID']              = $ID;
                    $juiInfo = juiGetJuiInfo();
                    $ret['juiInfoParts']    =& $juiInfo->getParts();
                    $ret['juiInfoPages']    =& $juiInfo->getPages();
                    $ret['juiInfoWidgets']  =& $juiInfo->getWidgets();
                    $ret['curPart']         =& $juiInfo->getCurPart();
                    $ret['curPage']         =& $juiInfo->getCurPage();
                    $ret['getCurWidget']    =& $juiInfo->getCurWidget();

                    return $ret;
                }


    function prepareJuiVars($juiid, &$data, &$renderer) {
        global $ID;

                //JUI-FIX::
//                juiDbgMsg("..........prepareJuiVars.juiid=$juiid", 1);
        // get the scheme
        // fall back to default if no valid scheme got
        $plugin = $this->getPluginName();   // juiacc, juitabs, ...
        $scheme = $this->getSchemeInstance($data['opts'][$plugin]['scheme']);
        if ( !$scheme->hasData() ) {
            // there should be a scheme with data
            juiDbgMsg("Error loading scheme: " . $scheme->getSchemeName());
            return false;
        }

        // prepare vars for client (JavaSript) and server (PHP)
        $jui_jsvars = $scheme->getVarsJS($data, $plugin);
        $this->prepareJuiVarsAfter($jui_jsvars);
        
        $jui_jsvars['page'] = $ID;
        $jui_jsvars['plugin'] = $plugin;
        $jui_depends = $this->_getWidgetDepends(array('scheme' => $scheme));
        $jui_subpages = $this->_getWidgetSubpages($data['sections']);

        $juiInfo = juiGetJuiInfo();
        // get the widget, create if not exists
        $widget =& $juiInfo->getWidget($juiid, true);
        // add the widget to the current page
        $page =& $juiInfo->addWidget($widget);
        if (!$page) {
            juiDbgMsg("Error addWidget('$juiid') - prepareJuiVars()", -1);
            return false;
        }

        $widget['plugin'] = $plugin;
        $page['rebuild'] = true;
        if ($this->isPreview() || $this->isSyspage()) {
            $widget['jsvars'] = $jui_jsvars;
            $page['juivars']  = $juiInfo->getJuiVarsPage($ID);
        } else {
            if (!isset($widget['jsvars'])) $widget['jsvars'] = $jui_jsvars;
        }
        
        if (!empty($jui_depends)) {
            if (!empty($widget['depends'])) {
                $ddiff = array_diff($jui_depends, $widget['depends']);
            }
            $widget['depends'] = empty($ddiff)
                                     ? $jui_depends
                                     : array_merge($jui_depends, $ddiff);
        }
        if (!empty($jui_subpages)) $widget['subpages'] = $jui_subpages;
    }

    public function prepareJuiVarsAfter(&$jsvars) {}
    
    public function ajaxSetCache(&$renderer) {
        if (juiGetIsAjaxCall()) {
            $renderer->info['cache'] = false;   //do not save cachefile
        }
    }

    /**
     * from ...\plugins\include\syntax\include.php
     *
     * @param <array> $sections
     * @return <array> included subpages in that widget
     */
    private function _getWidgetSubpages($sections) {
        $ret = array();

        foreach ($sections as $section) {      //JUIFIX:: _getWidgetSubpages
            $pattern = '/{{page>.+?}}/';
            preg_match_all($pattern, $section['content'], $matches);
            foreach($matches[0] as $match){
                $xx = 0;
                $match = substr($match, 2, -2); // strip markup
                list($match, $foo) = explode('&', $match, 2);

                // break the pattern up into its constituent parts
                list($include, $id, $section) = preg_split('/>|#/u', $match, 3);
                $id = cleanID($this->_includeApplyMacro($id));
                if (in_array($id, $ret)) {
                    $key = array_search($id, $ret);
                    $ret[$key] = $id . '&multi';
                } else {
                    $ret[] = $id;
                }
            }
        }

        return $ret;
    }

    /**
     * Copy from ...\plugins\include\syntax\include.php
     * 
     * Makes user or date dependent includes possible
     */
    private function _includeApplyMacro($id) {
        global $INFO, $auth;

        $user     = $_SERVER['REMOTE_USER'];
        $userdata = $auth->getUserData($user);
        $group    = $userdata['grps'][0];
                    //JUIFIX:: bitte aktualisieren -> function _apply_macro($id)
        $replace = array(
                '@USER@'  => cleanID($user),
                '@NAME@'  => cleanID($INFO['userinfo']['name']),
                '@GROUP@' => cleanID($group),
                '@YEAR@'  => date('Y'),
                '@MONTH@' => date('m'),
                '@DAY@'   => date('d'),
                );
        return str_replace(array_keys($replace), array_values($replace), $id);
    }





    // depends für ein einzelnes widget
    private function _getWidgetDepends($data) {
        $ret = array();

        $plugin = $this->getPluginName();   // juiacc, juitabs, ...
        $file = juiPathPluginRelative(JuiSchemes::getMetaFN($plugin, 'optsallowed'));
        if (!empty($file)) {
            $sn = "$plugin-meta.optsallowed";
            $ret[$sn] = $file;
        }

        $scheme = $data['scheme'];
        if ( isset($scheme) ) {
            $file = juiPathPluginRelative($scheme->getFileName());
            if (!empty($file)) {
                $sn = $scheme->getSchemeName();
                $ret[$sn] = $file;
                // base if exists
                $file = juiPathPluginRelative($scheme->getBaseFileName());
                if (!empty($file)) {
                    $sn = "$plugin-meta.base";
                    $ret[$sn] = $file;
                }
            }
        }

        $dscheme = $this->getSchemeInstance();  // default
        if (isset($dscheme) && $dscheme->getSchemeName() !== $scheme->getSchemeName()) {
            $file = juiPathPluginRelative($dscheme->getFileName());
            if (!empty($file)) {
                $sn = $dscheme->getSchemeName();
                $ret[$sn] = $file;
            }
        }

        return $ret;
    }


    /**
     * Returns the specified local text in raw format
     *
     */
    function rawLocale($id){
        return io_readFile($this->localFN($id));
    }


    //
    // Testfunctions
    //
    public function testVars() {
        global $ID;
        $ret = array();

        $ret['ID']              = $ID;
        $juiInfo = juiGetJuiInfo();
        $ret['juiInfoParts']    =& $juiInfo->getParts();
        $ret['juiInfoPages']    =& $juiInfo->getPages();
        $ret['juiInfoWidgets']  =& $juiInfo->getWidgets();
        $ret['curPart']         =& $juiInfo->getCurPart();
        $ret['curPage']         =& $juiInfo->getCurPage();

        return $ret;
    }


}

// vim:ts=4:sw=4:et:
