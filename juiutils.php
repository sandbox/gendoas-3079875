<?php
// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('JUI_UID_DELIMITER')) define('JUI_UID_DELIMITER','__0__');

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');

require_once DOKU_PLUGIN_JUI.'juiinfo.php';

function juiGetIsAjaxCall() {
//    $isAjax = $GLOBALS['juiINFO']['isAjaxCall'] ?? FALSE;
//    return $isAjax ? $isAjax : false;
    return (boolean) ($GLOBALS['juiINFO']['isAjaxCall'] ?? FALSE);
}
function juiSetIsAjaxCall() {
    $GLOBALS['juiINFO']['isAjaxCall'] = true;
}
function juiGetAjaxData() {
    global $juiINFO;
    return isset($juiINFO['ajaxData']) ? $juiINFO['ajaxData'] : false;
}
function juiSetAjaxData($data=false) {
    $GLOBALS['juiINFO']['ajaxData'] = $data;
}
function juiGetIsNesting($plugin) {
    $ret = $GLOBALS['juiINFO']['isNesting'][$plugin];
    return isset($ret) ? $ret : false;
}
function juiSetIsNesting($plugin, $value) {
    $ret = juiGetIsNesting($plugin);
    $GLOBALS['juiINFO']['isNesting'][$plugin] = $value;
    return $ret;
}

function juiGetJuiInfo() {
    return $GLOBALS['juiINFO']['JuiInfo'];
}
function &juiInfoSetCurPart($id) {
    return juiGetJuiInfo()->setCurPart($id);
}
function &juiInfoGetCurPart() {
    return juiGetJuiInfo()->getCurPart();
}
function juiInfoGetCurPartId() {
    $p = juiInfoGetCurPart();
    return $p ? $p['id'] : false;
}
function &juiInfoGetCurPage() {
    return juiGetJuiInfo()->getCurPage();
}

function juiInfoSetCurPartRendered() {
    $p =& juiInfoGetCurPart();
    if (!$p) return false;

    $p['rendered'] = true;
    return true;
}

function juiExistsJuiPlugins() {
    global $juiINFO;
    if ($juiINFO && is_array($juiINFO['juiplugins'])) {
        return (count($juiINFO['juiplugins']) > 0);
    }
    return false;
}
function juiGetJuiPlugins() {
    return $GLOBALS['juiINFO']['juiplugins'];
}
function juiSetJuiPlugins($plugin) {
    global $juiINFO;
    if (!is_array($juiINFO)) $juiINFO = array();
    if (!is_array($juiINFO['juiplugins'])) $juiINFO['juiplugins'] = array();
    if (!in_array($plugin, $juiINFO['juiplugins'])) {
        $juiINFO['juiplugins'][] = $plugin;
    }
}



/**
 *
 * @param <string> $msg
 */
function juiDbgMsg($msg,$lvl=0,$line='',$file='') {
    global $conf, $INFO;
    if ( $conf['allowdebug'] && (!$INFO || $INFO['isadmin'])) {
            msg($msg, $lvl, $line, $file);
    }
}
function juiDbgMsgHtml($msg) {
    if ($GLOBALS['INFO']['isadmin'] && $GLOBALS['conf']['allowdebug']) {
        return $msg;
    }
    return '';
}

function juiCleanID($id) {
    return preg_replace('/__\d+__/', '', $id);
}
function juiPathPluginRelative($path) {
    $pattern = '#' . str_replace('\\', '\\\\', DOKU_PLUGIN) . '(.*)#';
    preg_match($pattern, $path, $matches);
    return !empty($matches[1]) ? $matches[1] : '';
}

/**
 *
 * @param string $opts, like xml-attributes
 * @param boolean $errmsg, msg on error, default=true
 * @return array, options as array, empty array on error
 */
function juiGetOptionsArray($opts, $allowed=false, $errmsg=true) {
    $helper =& plugin_load('helper', 'juiwidget');
    return $helper->getOptionsArray($opts, $allowed, $errmsg);
}

function juiRemoveEmptyArrays(&$target, $deep=false) {
    foreach ($target as $key => &$val) {
        if (!is_array($val)) continue;
        if (!empty($val) && $deep) juiRemoveEmptyArrays($val, true);
        if (empty($val)) unset($target[$key]);
    }
}

function juiInitAjaxReplace() {
    $GLOBALS['juiINFO']['ajaxReplace'] = array();
}
function juiAddAjaxReplace($replace) {
    if (!is_array($replace)) return;
    $ajaxReplace =& $GLOBALS['juiINFO']['ajaxReplace'];
    foreach($replace as $k => $v) {
        $k = str_replace(array('<', '>'), array('&lt;', '&gt;'), $k);
        if (!isset($ajaxReplace[$k])) $ajaxReplace[$k] = htmlspecialchars($v);
    }
}

function juiGetAjaxReplace() {
    $ret =& $GLOBALS['juiINFO']['ajaxReplace'];
    return !empty($ret) ? $ret : false;
}


function juiStateStore($key, $data) {
    $file = juiStateGetFN($key);
}

function juiStateRetrieve($key) {

}
function juiStateGetFN($key) {

}

function juiGetLang($id, $plugin = 'juiwidget') {
    global $juiINFO;
    if (!isset($juiINFO['lang'][$plugin])) {
        juiSetupLocale($plugin);
    }
    $lang = $juiINFO['lang'][$plugin];
    return (isset($lang[$id]) ? $lang[$id] : '');
}
function juiSetupLocale($plugin = 'juiwidget') {
    global $juiINFO;
    if (isset($juiINFO['lang'][$plugin])) return;

    global $conf;            // definitely don't invoke "global $lang"
    $path = DOKU_PLUGIN.$plugin.'/lang/';

    // don't include once, in case several plugin components require the same language file
    @include($path.'en/lang.php');
    if ($conf['lang'] != 'en') @include($path.$conf['lang'].'/lang.php');

    $juiINFO['lang'][$plugin] = $lang;
}

function juiGetRawLacale($id, $plugin = 'juiwidget'){
    return io_readFile(juiGetLocalFN($id, $plugin));
}

function juiGetLocalFN($id, $plugin = 'juiwidget') {
    global $conf;
    $file = DOKU_PLUGIN.$plugin.'/lang/'.$conf['lang'].'/'.$id.'.txt';
    if(!@file_exists($file)){
        //fall back to english
        $file = DOKU_PLUGIN_JUI.'lang/en/'.$id.'.txt';
    }
    return $file;
}

//                function juiApplyMacro($text) {
//                    $replace = array(
//                            '@PAGEID@'  => cleanID($user),
//                            '@NAME@'  => cleanID($INFO['userinfo']['name']),
//                            '@GROUP@' => cleanID($group),
//                            '@YEAR@'  => date('Y'),
//                            '@MONTH@' => date('m'),
//                            '@DAY@'   => date('d'),
//                            );
//                    return str_replace(array_keys($replace), array_values($replace), $text);
//                }

// http://de2.php.net/manual/de/function.array-merge-recursive.php
//function array_merge_recursive_simple() {
function juiArrayMergeRecursiveSimple() {

    if (func_num_args() < 2) {
        trigger_error(__FUNCTION__ .' needs two or more array arguments', E_USER_WARNING);
        return;
    }
    $arrays = func_get_args();
    $merged = array();
    while ($arrays) {
        $array = array_shift($arrays);
        if (!is_array($array)) {
            trigger_error(__FUNCTION__ .' encountered a non array argument', E_USER_WARNING);
            return;
        }
        if (!$array)
            continue;
        foreach ($array as $key => $value)
            if (is_string($key))
                if (is_array($value) && array_key_exists($key, $merged) && is_array($merged[$key]))
                    $merged[$key] = call_user_func(__FUNCTION__, $merged[$key], $value);
                else
                    $merged[$key] = $value;
            else
                $merged[] = $value;
    }
    return $merged;
}


?>
