$jui = $jui || {};

$jui.sbtoggler = {
    jqelem: false
,   exists: false
,   sidebar: false
,   content: false
,   lockable: false
,   pinable: false
,   minheight: 0
,   create: function(data) {
        var sidebar = data.sidebar
        ,   content = data.content
        ,   self    = this
        ,   state   = { wsidebar: $jui.misc.widthPcnt(sidebar, 0)
                      , wcontent: $jui.misc.widthPcnt(content, 0)
                      , pglocked: false
                      , pinned:   false
                      , hidden:   false
                      }
        ;
        self.sidebar = sidebar;
        self.content = content;
        self.lockable = data.lockable;
        self.pinable = data.pinable;


        sidebar.wrap(self._getHtmlWrap());
        self.jqelem = sidebar.after(self._getHtml()).next();
        self.exists = self.jqelem.jquery ? true : false;
        if (!self.exists) {
            $jui.dbgAlert('SB-Toggler not created');
            return;
        }

        self._bindButtons();
        self._bindSidebar(sidebar);
        content.addClass('jui_sbt_pcontent');

        self.setStateCur(state);
        self.pinSidebar(true);
        self.minheight = $jq('#jui_sbtoggler>.jui-sbt-icon').length * 16;
        self.resize();
    }
,   setCtrlFlag: function(active) {
        $jui.flags.keyCtrl = active;
        if (active) {
            this.jqelem.addClass('jui_sbt_marker');
                                //JUI-FIX:: ....tmp
                                $jq('.ui-state-active>.jui-tbwrench')
                                        .add('.jui-toolbar>.jui-tbwrench')
                                        .removeClass('ui-icon-none')
                                        .addClass('ui-icon-wrench');
        } else {
            this.jqelem.removeClass('jui_sbt_marker');
                                //JUI-FIX:: ....tmp
                                $jq('.ui-state-active>.jui-tbwrench')
                                        .add('.jui-toolbar>.jui-tbwrench')
                                        .removeClass('ui-icon-wrench')
                                        .addClass('ui-icon-none');
        }
    
    }
,   _getHtmlWrap: function() {return '<div id="jui_sbt_wrap"></div>';}
,   _getHtml: function() {
        var lockable = this.lockable
        ,   pinable  = this.pinable
        ,   action   = ['lock', 'open', 'tools', 'pin', 'help', 'none', 'hide']
        ,   ret      = '<div id="jui_sbtoggler">';
        $jq.each(['unlocked', 'folder-open', 'wrench', 'pin-w', 'help', 'none', 'circle-triangle-w']
        ,   function (index, value) {
                if (action[index] == 'lock' && !lockable) return;
                if (action[index] == 'pin'  && !pinable) return;
                ret += '<div id="jui_sbtoggler-' + action[index]
                     + '" class="jui-sbt-icon jui-icon-left ui-icon-' + value + '"></div>';
            }
        );
        return ret + '</div>';
    }
,   _bindButtons: function() {
        var self    = this;
        $jq('.jui-sbt-icon').click(
            function(event){
                $jui.selClose.fire();
                
                var action = $jui.get.actionFromID(this.id);
                switch (action) {
                  case 'lock':  action = 'toggleLocked';    break;
                  case 'open':  action = 'showOpen';        break;
                  case 'tools': action = 'showTools';       break;
                  case 'pin':   action = 'togglePinned';    break;
                  case 'help':  action = 'showHelp';        break;
                  default:      action = false;             break;
                }

                if (action) {
                    self[action]();
                    event.stopImmediatePropagation();
                }
            }
        );
        self.jqelem.click(
            function(event){
                $jui.selClose.fire();
                self.toggleHidden();
                event.stopImmediatePropagation();
            }
        );
    }
,   _bindSidebar: function(sidebar) {
        var self = this;
        sidebar.resize(self.resize);
        sidebar.mouseleave(function() {
            if (self.getStateCur().slide) { self.hideSidebar(true); }
        });
    }
,   resize: function(widget) {
        var self = this
        ,   part = widget ? widget.pgpart || {} : self.sidebar;
        if (!self.exists || part != self.sidebar) return;

        var th = self.jqelem.height()
        ,   sh = self.sidebar.outerHeight(true);
        sh = Math.max(sh, self.minheight);
        if (Math.abs(th - sh) > 10) {
            self.jqelem.height(sh);
        }
    }
,   getStateCur: function() { return this.getData('stateCur') || {}; }
,   setStateCur: function(data) { return this.setData('stateCur', data); }
,   getData: function(key) { return this.jqelem.data(key); }
,   setData: function(key, data) { return this.jqelem.data(key, data); }
,   getButtonJq: function(button) { return $jq('#jui_sbtoggler-' + button); }

,   isHidden: function() { return this.getStateCur().hidden || false; }
,   toggleHidden: function() { this.hideSidebar(!this.isHidden()); }
,   hideSidebar: function(hide) {
        var self  = this
        ,   state = self.getStateCur() || {}
        ,   pgstate = $jui.get.pgState()
        ;
        if (state.hidden == hide ) return;

        var sidebar = self.sidebar
        ,   button  = self.getButtonJq('hide')
        ,   cls_e   = 'ui-icon-circle-triangle-e'
        ,   cls_w   = 'ui-icon-circle-triangle-w'
        ,   data    = { content: self.content, width: state.wcontent }
        ;

        if (hide) {
            if (state.slide) {
                sidebar.hide('slide', null, 'slow' 
                ,   function() { sidebar.removeClass('jui_sbt_slide'); }
                );
                state.slide = false;
            } else {
                data.width += state.wsidebar;
                sidebar.hide('slide', null, 'slow'
                ,   function() { _resizeContent(data); }
                );
            }
        } else if (state.pinned) {
            _resizeContent(data);
            sidebar.show('slide',{},'slow',_resizeJuiChildrenSB);
            state.slide = false;
        } else {
            sidebar.addClass('jui_sbt_slide');
            sidebar.show('slide',{},'slow',_resizeJuiChildrenSB);
            state.slide = true;
        }

        button.removeClass(hide ? cls_w : cls_e)
              .addClass(hide ? cls_e : cls_w);
        state.hidden = hide;
        this.setStateCur(state);
        if (pgstate.locked && $jui.initialized) {
            pgstate.pinned = state.pinned;
            pgstate.hidden = state.hidden;
            $jui.ajax.storeState(JSINFO.id, pgstate);
        }
        return;

        function _resizeJuiChildrenSB() {
            var children = juiwindata.juiChildren;
            if (!children) return;

            var self = $jui.sbtoggler || {}
            ,   sb = self.sidebar;
            for (var id in children) {
                var ch = children[id];
                if (ch.pgpart != sb) continue;
                ch.juiResize();
            }
        }
        
        function _resizeContent(data) {
            try {
                var content  = data.content
                ,   children = juiwindata.juiChildrenContent
                ,   style    = {width: data.width + '%'};
                $jui.style.important(content, style, false);
                
                $jui.lockHeight = true;
                for (var id in children) {
                    children[id].juiResize();
                }
                delete $jui.lockHeight;
            }catch(e){
                $jui.dbgAlert('_resizeContent', e.message);
            }
        }
    }

,   isPinned: function() { return this.getStateCur().pinned || false; }
,   togglePinned: function() { 
        this.pinSidebar(!this.isPinned());
        this.hideSidebar(true);
    }
,   pinSidebar: function(pin) {
        var button = this.getButtonJq('pin')
        ,   state = this.getStateCur() || {}
        ,   pgstate = $jui.get.pgState()
        ;
        if (state.pinned == pin ) return;

        if (pin) {
            state.pinned = true;
            button.removeClass('ui-icon-pin-w')
                  .addClass('jui_sbt_marker')
                  .addClass('ui-icon-pin-s')
        } else {
            state.pinned = false;
            button.removeClass('ui-icon-pin-s')
                  .removeClass('jui_sbt_marker')
                  .addClass('ui-icon-pin-w')
        }
        this.setStateCur(state);
        if (pgstate.locked && $jui.initialized) {
            pgstate.pinned = state.pinned;
            pgstate.hidden = state.hidden;
            $jui.ajax.storeState(JSINFO.id, pgstate);
        }
//        try {
//        }catch(e){}
    }

,   isLocked: function() { return this.getStateCur().pglocked || false; }
,   toggleLocked: function() { this.lockPage(!this.isLocked()); }
,   lockPage: function(lock) {
        var button = this.getButtonJq('lock')
        ,   state = this.getStateCur() || {}
        ,   pgstate = $jui.get.pgState()
        ,   marker  = 'jui_sbt_marker'
        ,   mmax    = 3;
        ;
        if ($jui.flags.keyCtrl) {
            if (pgstate.locked) pgstate.locked++;
            if (!pgstate.locked || pgstate.locked > mmax) pgstate.locked = 2;
            $jui.ajax.storeState(JSINFO.id, pgstate);
            marker = 'jui_sbt_marker' + pgstate.locked;
            if (state.pglocked) {
                _setMarkerClass(button, marker)
                return;
            }
        } else if (pgstate.locked) {
            pgstate.locked = false;
            delete pgstate.pinned;
            delete pgstate.hidden;
            $jui.ajax.storeState(JSINFO.id, pgstate);
        }
        if (state.pglocked == lock ) return;

        if (lock) {
            state.pglocked = true;
            _setMarkerClass(button, marker)
            button.removeClass('ui-icon-unlocked')
                  .addClass('ui-icon-locked')
        } else {
            state.pglocked = false;
            _setMarkerClass(button, false)
            button.removeClass('ui-icon-locked')
                  .addClass('ui-icon-unlocked')
        }
        if (!this.pinable) {
            state.pinned = !lock;
            this.setStateCur(state);
            this.hideSidebar(lock);
        } else {
            this.setStateCur(state);
        }
        if (pgstate.locked && $jui.initialized) {
            pgstate.pinned = state.pinned;
            pgstate.hidden = state.hidden;
            $jui.ajax.storeState(JSINFO.id, pgstate);
        }
        
        
        function _setMarkerClass(button, marker) {
            button.removeClass('jui_sbt_marker jui_sbt_marker2 jui_sbt_marker3');
            if (marker) button.addClass(marker);
        }
    }
,   showOpen: function() {
        if (confirm('Ausgewählte Seiten werden geöffnet.') !== false) {
            var file = 'lib/plugins/juiwidget/ajax_test_get.php'
            ,   opts = '?type=json&r_key=pages'
            ,   url = DOKU_BASE + file + opts
            ,   data = ''
            ;
            $jq.getJSON(url, data,
                function(data) {
//                    alert('Daten erhalten von: ' + url);
                    if (!$jq.isArray(data) || data.length < 1) return;

                    for (var item in data) {
                        var page = data[item]
                        ,   href    = DOKU_BASE + 'doku.php?id=' + page
                        ;
                        if (!page) break;
                        
                        window.open(href, '_blank');
                    }
                }
            );
////                    var page    = 'start'
////        //            ,   href    = DOKU_BASE + 'doku.php?id=' + page
////                    ,   href    = DOKU_BASE
////                    ,   wname   = 'workspace:xx'
////                    ,   opts    = 'scrollbars=1,menubar=0,toolbar=0,modal=0'
////                    ,   wneu    = window.open(href, wname, opts);
////                    wneu.focus();
        }
    }
,   showTools: function() {
        alert('$jui.sbtoggler.showTools');
    }
,   showHelp: function() {
        alert('$jui.sbtoggler.showHelp');
    }
};


