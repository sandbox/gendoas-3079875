<?php
/**
 * Counter for DokuWiki Plugin juiwidget
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */
class JuiCounter {

    private static $_uidCounter = 0;

    // prevent creating instance
    private function __construct() {
    }

    /**
     * Reset uid-counter
     *
     * @param integer $start (default=0)
     * @return integer last uid, before resetting
     */
    static function uidReset($start=0) {
        $ret = self::$_uidCounter;
        self::$_uidCounter = $start;
        return $ret;
    }

    /**
     * Increment the uid-counter and return it
     *
     * @return integer
     */
    static function uidNext() {
        return ++self::$_uidCounter;
    }

    /**
     * Return the last value of the uid-counter
     *
     * @return integer
     */
    static function uidLast() {
        return self::$_uidCounter;
    }
}

?>
