<?php
/**
 * Metadata for configuration manager plugin
 * Additions for the JuiWidget Plugin
 *
 * @author    TODO:: Esther Brunner <wikidesign@gmail.com>
 */
$meta['juidatadir'] = array('string');
$meta['juitoggler'] = array('onoff');
$meta['juitoggler_pinable'] = array('onoff');
$meta['juitoggler_lockable'] =  array('onoff');
$meta['hide_pagename']  = array('onoff');
$meta['insert_pageicon']  = array('onoff');

//Setup VIM: ex: et ts=2 enc=utf-8 :
