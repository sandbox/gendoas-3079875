<?php
/**
 * Options for the JuiWidget Plugin
 */
$conf['juidatadir']  = 'juidata';		//
$conf['juitoggler']  = true;		//
$conf['juitoggler_pinable']  = false;		//
$conf['juitoggler_lockable'] = true;		//
$conf['hide_pagename']  = false;		//
$conf['insert_pageicon']  = false;		//

//Setup VIM: ex: et ts=2 enc=utf-8 :