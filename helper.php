<?php
/**
 * DokuWiki Plugin juihelper (Helper Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin.spreizer@web.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_LF')) define('DOKU_LF', "\n");
if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

class helper_plugin_juiwidget extends DokuWiki_Plugin {

    /**
     *
     * @param string $opts, like xml-attributes
     * @param boolean $errmsg, msg on error, default=true
     * @return array, options as array, empty array on error
     */
    function getOptionsArray($opts, $allowed=false, $errmsg=true) {

        if ( !$allowed ) {
            return array();     // return empty array, error-msg is done
        }
        if ( ($xml = $this->_getXmlFromOps($opts, $errmsg)) === false ) {
            return array();     // return empty array, error-msg is done
        }

        //
        $ret = array();
        $array_needed = array('opts', 'attr', 'style');
        $list_allowed = array('addClass', 'removeClass', 'addFlags', 'removeFlags');
        $allowed_all = ($allowed === '*');
        if ($allowed_all) $allowed = array();
        foreach($xml->attributes() as $key => $val) {
            if ($allowed_all) $allowed[$key] = '*';

            // skip not allowed option
            if (!isset($allowed[$key] ) ) continue;

            $value = trim("$val");    // obj to string
            if ( empty($value) ) continue;

            if ($key == 'scheme' && strpos($value, ':') > 0 ) {
                list($value, $ext) = array_map('trim', explode(':', $value, 2));
                if ($ext != '') $ret['schemeExt'] = $ext;
            }

            if ( in_array($key, $array_needed) ) {
                $value = $this->_splitOptValue($value, $allowed[$key], $key);
            } elseif ( is_array($allowed[$key]) ) {
                if ( in_array($key, $list_allowed) ) {
                    $aval = array_map('trim', explode(' ', $value));
                    $value = implode(' ', array_intersect($aval, $allowed[$key]));
                } elseif ( !in_array($value, $allowed[$key]) ) {
                    continue;
                }
            }


            if ( !empty($value) || ($value !== 0) ) {
                $ret[$key] = $value;
            }
        }

        return $ret;
    }

    private function _getXmlFromOps($opts, $errmsg) {
        if ( empty($opts) ) {
            return false;
        }

        $keep = libxml_use_internal_errors(true);
        $xml = simplexml_load_string("<foo " . $opts . " />");
        if ($xml===false && $errmsg) {
            $msg = "Error loading options: $opts \n";
            foreach(libxml_get_errors() as $error) {
                $msg .=  "\n\t" . $error->message;
            }
            msg( $msg );
        }
        
        libxml_use_internal_errors($keep);
        return $xml;
    }

    private function _splitOptValue($str, $allowed, $optskey) {

        // get an assoc array from string
        // eg. 'collapsible:false; active:-1'
        // gets array('collapsible' => false, 'active' => -1)
        $ret = array();
        $items = explode(';', $str);
        $allowed_all = ($allowed === '*');
        foreach ( $items as $item ) {
            unset($val);      // reset
            list($key, $val) = array_map('trim', explode(':', $item, 2));
            if ( empty($key) || !isset($val) ) continue;
            if (strpos($val, 'function') !== false) continue;   // function not allowed

            $val = $this->_getValFromStr($val); // 'false'->false, '1'->1

            if ($allowed_all) $allowed[$key] = '*';
            $testkey = $key;
            if ($optskey == 'style') {
                list($testkey, $foo) = explode('-', $key, 2);
            }
            if (isset($allowed[$testkey])
            && $this->_isRequiredType($val, $allowed[$testkey]) ) {
                // add a valid option
                $ret[$key] = $val;
            }
            unset($val);
        }
        return $ret;
    }

    private function _getValFromStr($str) {
        switch (strtolower($str)) {
            case 'true':
                return true;

            case 'false':
                return false;

            case 'null':
                return null;

            default:
                if (is_numeric($str)) {
                    // Return float or int, as appropriate
                    return ((float)$str == (integer)$str)
                        ? (integer)$str
                        : (float)$str;

                }

                return $str;
        }
    }

    private function _isRequiredType($var, $required) {

        if (!isset($required)) return false;
        if (is_array($required)) {
		return in_array($var, $required);
	   }
        if (!is_string($required)) return false;

        $items = array_map('trim', explode('||', $required));
        foreach ( $items as $item ) {
            switch (strtolower($item)) {
                case '*':           return true;
                case 'bool':        return is_bool($var);
                case 'int':         return is_int($var);
                case 'float':       return is_float($var);
                case 'string':      return is_string($var);
            }
        }

        return false;
    }


}

// vim:ts=4:sw=4:et:
