<?php
$lang['jui_widgets'] = 'JUI-Elemente';
$lang['juiacc_title'] = 'JUI-Accordion';
$lang['juilayout_title'] = 'JUI-Layout';
$lang['juitabs_title'] = 'JUI-Tabs';

$lang['pages'] = 'Seiten';
$lang['more'] = 'Weitere';


$lang['js']['btn_next']             = 'Weiter'; 
$lang['js']['btn_back']             = 'Zurück';
$lang['js']['btn_cancel']           = 'Abbrechen';
$lang['js']['btn_apply']            = 'Übernehmen';
$lang['js']['btn_preview']          = 'Vorschau';
?>
