<?php
$lang['juidatadir'] = 'Speicherverzeichnis für Jui-Elemente';
$lang['juitoggler'] = 'Ausblenden der linken Navigationsleiste ermöglichen';
$lang['juitoggler_pinable'] = 'Festhalten der Navigationsleiste ermöglichen';
$lang['juitoggler_lockable'] = 'Sperren der Seite ermöglichen';
$lang['hide_pagename']  = 'Verberge Seitenname (HeaderPagename)';
$lang['insert_pageicon']  = 'Zeige Seiten-Icon (BarTop)';
?>
