<?php
$lang['jui_widgets'] = 'JUI-Widgets';
$lang['juiacc_title'] = 'JUI-Accordion';
$lang['juilayout_title'] = 'JUI-Layout';
$lang['juitabs_title'] = 'JUI-Tabs';

$lang['pages'] = 'Pages';
$lang['more'] = 'More';

$lang['js']['btn_next']             = 'Next'; 
$lang['js']['btn_back']             = 'Back';
$lang['js']['btn_cancel']           = 'Cancel';
$lang['js']['btn_apply']            = 'Apply';
$lang['js']['btn_preview']          = 'Preview';
?>
