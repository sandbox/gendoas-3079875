/*!
 * included JQuery plugins:
 *      - JSizes - JQuery plugin v0.33
 */
/* DOKUWIKI:include_once jquery.sizes.js */

/*!
 * jui includs:
 *      - jui.initvars: zuerst die Variabeln erzeugen (vor anderem include)
 *      - Sidebar Toggler
 */
/* DOKUWIKI:include jui.initvars.js */
/* DOKUWIKI:include jui.sbtoggler.js */

var $jq = null;
var intervalID = setInterval(jQueryInit);

function jQueryInit() {
  if (jQuery || false) {
    clearInterval(intervalID);
    intervalID = null;

    $jq = jQuery.noConflict();
    jQueryExtend();
    juiMainExtend();

    $jq(document).ready(function () {
      juiInitialize();
      juiOnJqReady('juitabs', {});
      juiOnJqReady('juilayout', {});
      juiOnJqReady('juiacc', {});
    });
  }
}

//
$jui.$jq = {
//    init: function() {}
  isNumeric: function (obj) {
    return !isNaN(parseFloat(obj)) && isFinite(obj);
  }
};

$jui.arr = {
  isEmpty: function (obj) {
    if (!obj)
      return true;
    for (var k in obj) {
      return false;
    }
    return true;
  }
  , count: function (obj) {
    if (!obj)
      return false;
    var ret = 0;
    for (var k in obj) {
      ++ret;
    }
    return ret;
  }
};

// extend jQuery
function jQueryExtend() {
  $jq.browser = {
    mozilla: true
    , webkit: false
    , msie: false
    , isIE6: false
    , boxModel: false
  }
  $jq.curCSS = function (element, prop, val) {
    return $jq(element).css(prop, val);
  };
}

// extend $jui, the main jui-object
function juiMainExtend() {

  $jq.extend($jui, {
    version: '0.0.1'
    , initialized: false
//  , debug: false
    , debug: true
    , dbgAlert: function (msg1, msg2) {
//    if (this.debug)
      if (true)
        alert('Error: ' + msg1 + "\n" + msg2);
    }
    , onJqReady: function (plugin, data) {
      juivars = juivars || null;
      var winvars = juivars ? juivars.window || {} : {};
      if (!winvars.widgetsExists) {
        return;
      }

      try {
        // initialize jui-widgets
        _initJuiVars(data);

        //JUITMP::
        // temp: var tbwrench = $jq('.jui-tbwrench', ...
        _initTmp(data);

        // call plugin func
        $jui[plugin].onJqReady(data);

        // waiting widgets (outer first, than inner widgets)
        var inner = false;
        for (i = 0; i < juiwindata.juiPlugins.length; i++) {
          inner = juiwindata.juiPlugins[i];
          if (plugin == inner)
            continue;

          if (juiwindata.waiting[inner]) {
            $jui[inner].onJqReady(data);
          }
        }

        if ($jui.page.isPreview()) {
          // make resizer visible
          $jq('.ui-resizable-handle').addClass('jui-preview');
        }
      } catch (e) {
        if (inner)
          plugin += '->' + inner;
        var msg = "jui[" + plugin
            + "].onJqRready('context='"
            + data['context'] + "')";
        $jui.dbgAlert(msg, e.message);
      }

      function _initTmp(data) {
        var tbwrench = $jq('.jui-tbwrench', data['context']);
        tbwrench.click(
            function (event) {
              $jui.selClose.fire();
              var target = this
                  , widget = $jui.widget.getSelf($jui.widget.getJqElem(target)) || false
                  , funcs = widget.juiwfuncs || false;
              if (funcs && funcs.tbwrenchClick) {
                var data = funcs.tbwrenchClick({widget: widget}) || {}
                , msg = data.msg || 'Error: No Msg!'
                    , ajaxpageid = data.ajaxpageid;
                if (ajaxpageid) {
                  var refresh = $jui.flags.keyCtrl
                      , action = refresh ? 'Inhalt aktualisieren'
                      : 'Seite bearbeiten';

                  msg = action + ":\n   " + ajaxpageid + "\n\n" + msg;
                  if (confirm(msg) !== false) {
                    if (refresh) {
                      // flag-anzeige entfernen
                      $jui.sbtoggler.setCtrlFlag(false);
                      // Inhalt aktualisieren
                      var cid = widget.curContent ? widget.curContent[0].id : false
                          , ajaxdata = widget.ajax && cid ? widget.ajax[cid] : false
                      if (ajaxdata) {
                        ajaxdata.refresh = true;
                        $jui.ajax.parseContent(widget, widget.curContent);
                      }
                    } else {
                      var href = DOKU_BASE + 'doku.php?id='
                          + ajaxpageid + '&do=edit&rev=';
                      if ($jui.page.isLocked() || $jui.page.isPreview()) {
                        $jui.win.openExtend(href);
                      } else {
                        //   //                                    window.open(href, '_blank');
                        window.open(href, '_self');
                      }
                    }
                  }
                  if ($jui.flags.keyCtrl)
                    $jui.sbtoggler.setCtrlFlag(false);
                } else {
                  alert(msg);
                }


              }
              event.stopImmediatePropagation();
            });
        tbwrench.mouseover(
            function (event) {
              $jq(this).removeClass('ui-icon-none')
                  .addClass('ui-icon-wrench');
            });
        tbwrench.mouseout(
            function (event) {
              //JUI-FIX:: ....tmp
              if ($jui.flags.keyCtrl)
                return;
              $jq(this).removeClass('ui-icon-wrench')
                  .addClass('ui-icon-none');
            });
      }


      function _initJuiVars(data) {
        if (!(juivars && juivars.window)) {
          return;
        }

        try {
          if (data.ajaxcall) {
            if (!juiwindata.widgetsExists)
              juiwindata.widgetsExists = {};
            if (!juiwindata.widgetsIDs)
              juiwindata.widgetsIDs = [];

            // for ajaxcall we habe to extend data
            // 1. widgetsExists
            if (juivars.window.widgetsExists) {
              for (var p in juivars.window.widgetsExists) {
                juiwindata.widgetsExists[p] = true;
              }
            }

            // 2. widgetsIDs
            var wIDs = juivars.window.widgetsIDs;
            if (wIDs) {
              for (var i = 0; i < wIDs.length; i++) {
                var wid = wIDs[i];
                if ($jq.inArray(wid, juiwindata.widgetsIDs) == -1) {
                  juiwindata.widgetsIDs.push(wid);
                }
              }
            }

            // 3. uidLast
            if (juivars.window.uidLast > juiwindata.uidLast) {
              juiwindata.uidLast = juivars.window.uidLast;
            }
          }

          // init data storage for each widget
          _initWidgetData(data);
        } catch (e) {
          $jui.dbgAlert('_initJuiVarsAjaxcall', e.message);
        }
      }

      function _initWidgetData(data) {
        try {
          //  each widget its own storage
          var widgets = juivars.widgets
              , parts = juiwindata.pgparts;
          for (var i = 0; i < juiwindata.juiPlugins.length; i++) {
            var plugin = juiwindata.juiPlugins[i];
            $jq('.' + plugin + '-widget', data.context).each(function () {
              var cid = $jui.widget.getCleanID(this['id']);
              if (cid && widgets[cid]) {
                var $widget = $jq(this)
                    , plugin = widgets[cid].plugin
                    , jopts = widgets[cid][plugin] || {}
                , juiflags = jopts.addFlags || []
                    , wpart = false
                    ;
                $widget.data('uid', this['id']);
                $widget.data('juiflags', juiflags);
                if (!widgets[cid]['uid']) {
                  widgets[cid]['uid'] = this['id'];
                  $widget.data('juivars', widgets[cid]);
                } else {
                  // make a copy
                  $widget.data('juivars', $jq.extend(true, {}, widgets[cid]));
                }

                for (var part in parts) {
                  part = parts[part];
                  var p = $widget.parentsUntil(part.parent());
                  if (p[p.length - 1].className == part[0].className) {
                    wpart = part;
                    break;
                  }
                }
                if (!wpart) {   // maybe a dialog
                  wpart = $widget.closest('.juidialog');
                }
                if (!wpart) {
                  $jui.dbgAlert('pgpart not found: ' + this['id'], '');
                }
                $widget.data('pgpart', wpart);
              } else {
                var msg = '_initWidgetData(): no widget-data !!'
                    + "\n  cid=" + cid
                    + "\n  widgets[cid]=" + widgets[cid];
                $jui.dbgAlert(msg, '');
              }
            });
          }
        } catch (e) {
          $jui.dbgAlert('_initWidgetData', e.message);
        }
      }
    }
  });

  $jui.LANG = $jq.extend({}, LANG.plugins.juiwidget, LANG.plugins.juiacc, LANG.plugins.juilayout, LANG.plugins.juitabs);
}

//
$jui.page = {
  isLocked: function () {
    var sbt = $jui.sbtoggler || {}
    , state = sbt.exists ? sbt.getStateCur() || {} : {};
    return state.pglocked ? state.pglocked : false;
  },
  isPreview: function () {
    var ret = juiwindata.cur ? juiwindata.cur.isPreview || false : false;
    return ret;
//        return (juiwindata.cur ? juiwindata.cur.isPreview || false : false);
  }
};

$jui.selClose = {
  list: []
  , timeout: false
  , add: function (elem) {
    if (!elem.jquery)
      return;
    if (!this.has(elem))
      this.list.push(elem);
  }
  , has: function (elem) {
    var list = this.list
        , length = list.length;
    if (!elem) {
      return length > 0;
    }
    for (var i = 0; i < length; i++) {
      if (elem === list[i]) {
        return true;
      }
    }
    return false;
  }
  , fire: function () {
    var list = this.list
        , length = list.length;
    if (!list || !length)
      return;
    for (var i = 0; i < length; i++) {
      if (!list[i].data('pinned')) {
        list[i].hide();
      }
    }
    this.list = [];
  }
  , setTimeout: function (time) {
    this.clearTimeout();
    this.timeout = setTimeout("$jui.selClose.fire()", time || 5000);
  }
  , clearTimeout: function () {
    if (!this.timeout)
      return;
    clearTimeout(this.timeout);
    this.timeout = false;
  }
}

$jui.win = {
  onResize: function () {
    if (juiwindata.JuiResizing)
      return;

    var jqwin = $jq(window)
        , h = jqwin.height()
        , w = jqwin.width();
    if (juiwindata.clientHeight == h && juiwindata.clientWidth == w) {
      return;
    }

    juiwindata.clientHeight = h;
    juiwindata.clientWidth = w;

    if (!juiwindata.juiChildrenSbLeft) {
      $jui.sbtoggler.resize();
    }

    var children = juiwindata.juiChildren;
    if (!children)
      return;

    for (var id in children) {
      children[id].juiResize();
    }
  }
  , onKeydown: function (e) {
//        if (e.keyCode == 27) {
    if (e.keyCode == $jq.ui.keyCode.ESCAPE) {
      $jui.selClose.fire();
      return;
    }
  }
//,   closeCallbacks: $jq.Callbacks ? $jq.Callbacks('once unique') : {}
  , delegateBodyClick: function () {
    if ($jui.selClose.has()) {
      $jui.selClose.fire();
    }
  }
  , delegateAClick: function () {
    var sbt = $jui.sbtoggler;
    if (!sbt.exists)
      return;

    var baseURI = this.baseURI ? this.baseURI : document.URL
        , len = baseURI.length
        , state = sbt.getStateCur();
    if (!state.pglocked || this.target != '')
      return;

    if (this.href.substring(0, len) == baseURI && this.href[len] == '#') {
      return;
    }

    if (state.slide)
      sbt.hideSidebar(true);


    var page = _getIPage(this);
    if (page) {
      if ($jui.ajax.parsePageContent(page)) {
        return false;
      }
    }

    $jui.win.openExtend(this.href);
    return false;


    function _getIPage(elem) {
      var baseURI = this.baseURI ? this.baseURI : document.URL
          , href = elem.href || false;
      if ($jui.get.pgState().locked !== 3
          || !$jq(elem).closest('.left_sidebar').length
          || !href) {
        return false;
      }

      href = href.split('?');
      if (baseURI.split('?')[0] != href[0] || !href[1])
        return false;

      var ps = href[1].split('&')
          , page = ps[0] || '';
      if (ps.length > 1 || page.split('=')[0] != 'id')
        return false;

      page = page.split('=');
      return page[0] == 'id' && page[1] ? page[1] : false;
    }
  }
  , openExtend: function (href) {
    var wname = window.name == '' ? 'juiExtend' : window.name + 'X'
        , opts = 'scrollbars=1,menubar=0,toolbar=0,modal=1'
        , wneu = window.open(href, wname, opts);
    wneu.focus();
  }
};

$jui.ajax = {
  storeState: function (key, data) {
    if ($jui.page.isPreview())
      return;
    if (data.foo !== undefined)
      delete data.foo;
    $jui.ajax.storeData('state', key, data);
  }
  , storeVote: function (key, data) {
    $jui.ajax.storeData('vote', key, data);
  }
  , storeData: function (target, key, data) {
    try {
      var juidata = {target: target,
        key: key,
        juidata: JSON.stringify(data)
      };
      $jq.post(DOKU_BASE + 'lib/plugins/juiwidget/ajax_store.php', juidata);
    } catch (e) {
      $jui.dbgAlert('jui.ajax.storeData()', e.message);
    }
  }
  , parsePageContent: function (page, target, extra) {
    try {
      extra = extra || false;
      target = target || $jq('.right_page.jui_sbt_pcontent');
      var content = '<juiajax call="page" page=":' + page + '">';
      if (!target[0])
        return false;

      // show loading...
      $jui.misc.showLoading(target);

      var juidata = {content: content,
        uidLast: juiwindata.uidLast,
        extra: extra
      };
      juidata = {call: 'juiajax_parseContent',
        juidata: JSON.stringify(juidata)
      };

      if (target.juiAjaxPreLoad)
        target.juiAjaxPreLoad();
      $jq.post(DOKU_BASE + 'lib/exe/ajax.php',
          juidata,
          function (data) {
            try {
              target.html(data);

              data = {debug: $jui.debug,
                context: target,
                ajaxcall: true};
              for (var i = 0; i < juiwindata.juiPlugins.length; i++) {
                $jui.onJqReady(juiwindata.juiPlugins[i], data)
              }
              delete juivars;

              if (target.juiAjaxPostLoad)
                target.juiAjaxPostLoad();
            } catch (e) {
              $jui.dbgAlert('$jui.ajax.parseContent()', e.message);
            }
          }
      );
      return true; // no default action
    } catch (e) {
      $jui.dbgAlert('$jui.ajax.parsePageContent()', e.message);
    }
  }
  , parseContent: function (widget, target, extra) {
    try {
      extra = extra || false;
      widget = $jui.widget.getSelf(widget);
      target = target.jquery ? target : $jq(target);
      if (!widget[0] || !target[0])
        return;

      var cid = widget.curContent ? widget.curContent[0].id : false
          , content = false
          , ajaxdata = widget.ajax && cid ? widget.ajax[cid] : false
          , pattern = /^<juiajax .+>/;
      ;
      if (ajaxdata) {
        if (ajaxdata.source && ajaxdata.refresh) {
          content = ajaxdata.source.match(pattern);
        }
      } else {
        content = $jq.trim(target.text());
        content = content.match(pattern);
      }

      if (content) {
        // show loading...
        $jui.misc.showLoading(target);

        content = content[0];
        content = content.replace(/&quot;/g, '"');
        if (ajaxdata) {
          if (ajaxdata.refresh)
            delete ajaxdata.refresh;
        } else {
          // ajax-quelldaten für das objekt aufschreiben
          // ermöglicht 'aktualisieren', 'page-edit', ...
          if (!widget.ajax)
            widget.ajax = {};
          widget.ajax[cid] = {source: content, pageid: false};
          ajaxdata = widget.ajax[cid];
        }

        // jetzt also AjaxAufruf vorbereiten und ausführen
        var vars = widget.juivars
            , isNesting = []
            ;
        for (var i = 0; i < juiwindata.juiPlugins.length; i++) {
          var plugin = juiwindata.juiPlugins[i];
          if (false) {
            // keine Schachtellung erlaubt
            if (plugin == vars.plugin
                || target.closest('.' + plugin + '-widget').length > 0) {
              isNesting.push(plugin);
            }
          } else {
            // einmalige Schachtellung erlaubt
            var tc = target.closest('.' + plugin + '-widget');
            if (tc && tc.parent().closest('.' + plugin + '-widget').length > 0) {
              isNesting.push(plugin);
            }
          }
        }

        var juidata = {content: content,
          JSINFO: JSINFO,
          jsvars: vars,
          widgetsExists: juiwindata.widgetsExists,
          isNesting: isNesting,
          uidLast: juiwindata.uidLast,
          extra: extra
        };

        juidata = {call: 'juiajax_parseContent',
          juidata: JSON.stringify(juidata)
        };

        if (target.juiAjaxPreLoad)
          target.juiAjaxPreLoad();
        $jq.post(DOKU_BASE + 'lib/exe/ajax.php',
            juidata,
            function (data) {
              try {
                target.html(data);

                var ad = juivars ? juivars.ajaxData || {} : {};
                if (ad.pageid)
                  ajaxdata.pageid = ad.pageid;

                data = {debug: $jui.debug,
                  context: target,
                  ajaxcall: true};
                for (var i = 0; i < juiwindata.juiPlugins.length; i++) {
                  $jui.onJqReady(juiwindata.juiPlugins[i], data)
                }
                delete juivars;

                if (target.juiAjaxPostLoad)
                  target.juiAjaxPostLoad();
              } catch (e) {
                $jui.dbgAlert('$jui.ajax.parseContent()', e.message);
              }
            }
        );
      }
    } catch (e) {
      $jui.dbgAlert('$jui.ajax.parseContent()', e.message);
    }
  }
};


$jui.widget = {
  getID: function (id) {
    return id.split('-')[0];
  }
  , getCleanID: function (id) {
    return id.replace(/__\d+__/, '');
  }
  , getSelf: function (elem) {
    return $jq(elem).data('self');
  }
  , getJqElem: function (elem) {
    var id = elem.jquery ? $jui.widget.getID(elem[0].id) : $jui.widget.getID(elem.id);
    return $jq('#' + id);
  }
  , getClosest: function (elem) {
    var cls = elem.parent().closest('.juiwidget');
    return cls.length ? cls : false;
  }
  , getClosestContent: function (elem) {
    var cls = elem.parent().closest('.juicontent ');
    return cls.length ? cls : false;
  }
  , getClosestNotReady: function (elem) {
    var cls = $jui.widget.getClosest(elem)
        , plugin = elem.data('juivars').plugin
        , ret = cls ? !cls.data('ready') : false;
    if (ret)
      juiwindata.waiting[plugin] = true;
    return ret;
  }
  , setSectsAttrCss: function (elems, data, item) {
    if (item != 'head' && item != 'content')
      return;
    if (!elems.jquery || elems.length == 0 || $jq.isEmptyObject(data)) {
      return;
    }

    var self = this;
    elems.each(function () {
      var cid = $jui.get.contentIdFromID(this.id)
          , secdata = data[cid] || {};
      if (!secdata || !secdata[item])
        return;

      self.setAttrCss($jq(this), secdata[item]);
    });
  }
  , setAttrCss: function (elems, data) {
    if (!elems.jquery || elems.length == 0 || $jq.isEmptyObject(data)) {
      return;
    }

    if (data.addClass)
      elems.addClass(data.addClass);
    if (data.removeClass)
      elems.removeClass(data.removeClass);
    if (data.attr)
      elems.attr(data.attr);
    if (data.style) {
      elems.each(function () {
        $jui.style.mergeElem($jq(this), data.style, false);
      });
    }
  }
};

$jui.icon = {
  setPlusMinus: function (elem, plus) {
    elem = elem.jquery ? elem : $jq(elem);
    var rem = plus ? 'ui-icon-minus' : 'ui-icon-plus'
        , add = plus ? 'ui-icon-plus' : 'ui-icon-minus'
    elem.removeClass(rem).addClass(add);
  }
};

$jui.style = {
  toString: function (style) {
    var ret = '';
    for (var k in style) {
      var v = $jq.trim(style[k]);
      if (v != '' && v != '!important') {
        ret += k + ':' + style[k] + ';';
      }
    }
    return ret;
  }
  , toObject: function (style) {
    var ret = {}
    , ast = style.split(';');
    for (var i = 0; i < ast.length; i++) {
      var itm = ast[i].split(':');
      if (itm.length == 2) {
        ret[$jq.trim(itm[0])] = $jq.trim(itm[1]);
      }
    }
    return ret;
  }
  , merge: function (style, more) {
    var k, a, s
        , ret = typeof style === 'string'
        ? this.toObject(style)
        : $jq.extend({}, style || {})
        ;
    more = typeof more === 'string' ? this.toObject(more) : more;
    for (var k in more) {
      if (ret[k]) {
        a = $jq.trim(more[k]);
        s = $jq.trim(ret[k]);
        if (!!a.match(/ !important/) || !s.match(/ !important/)) {
          ret[k] = more[k];
        }
      } else {
        ret[k] = more[k];
      }
    }
    return ret;
  }
  , mergeElem: function (elem, more, clear) {
    if (elem.jquery) {
      var style = this.merge(elem.attr('style'), more);
      style = this.toString(style);
      style = clear ? style.replace(/ !important/g, '') : style;
      elem.attr('style', style);
    }
    ;
  }
  , important: function (elems, style, clear) {
    if (elems.jquery) {
      var self = this;
      for (var k in style) {
        var v = style[k];
        v = typeof v === 'number' ? v + 'px' : v;
        style[k] = v + ' !important';
      }
      elems.each(function () {
        self.mergeElem($jq(this), style, clear);
      });
    }
    ;
  }
};

$jui.flags = {};

$jui.get = {
  actionFromID: function (id) {
    return id.split('-')[1];
  }
  , contentIdFromID: function (id) {
    return id.split('-')[1];
  }
  , pgState: function () {
    return juiwindata.pgstate || {};
  }
  , usrState: function () {
    return juiwindata.usrstate || {};
  }
};


$jui.dialogs = {
  dialogs: {}
  , get: function (id, create, opts) {
    if (this.dialogs[id])
      return this.dialogs[id];
    if (!create)
      return false;

    // create
    var juidlgs = $jq('#juidialogs');
    if (!juidlgs.length) {
      $jq('body').append('<div id="juidialogs"></div>');
      juidlgs = $jq('#juidialogs');
      if (!juidlgs.length)
        return false;
    }

    opts = opts || {};
    var dopts = {title: 'Dialog Title',
      autoOpen: false,
      show: 0, // slide, scale, size, drop, blind
      modal: true
    };
    opts = $jq.extend(true, dopts, opts);
    juidlgs.append('<div id="' + id + '" class="dokuwiki"></div>');
    this.dialogs[id] = $jq('#' + id, juidlgs).dialog(opts);
    return this.dialogs[id];
  }

};



$jui.misc = {
  showLoading: function (target) {
    if (!target.jquery || !target.length)
      return;
    target.html('<img src="' + DOKU_BASE + 'lib/images/loading.gif" '
        + 'width="100" height="6" alt="..." />');

  }
  , heightPcnt: function (elem, p) {
    return parseFloat((elem.height() / elem.parent().height() * 100).toFixed(p));
  }
  , widthPcnt: function (elem, p) {
    return parseFloat((elem.width() / elem.parent().width() * 100).toFixed(p));
  }
  , source2opts: function (data) {
    //<juiajax call="page" page=":dev:acc:fs_tabs">
    if (typeof data != 'string')
      return {};

    data = $jq.trim(data);
    if (data.search(/\/>$/) == -1) {
      data = data.replace(/>$/, " />");
    }
    if (data.slice(0, 5) != "<?xml") {
      data = '<?xml version="1.0" ?>' + data;
    }

    var ret = {}
    , elem = $jq.parseXML(data);

    if (elem) {
      var attribs = elem.firstChild.attributes;
      for (var i = 0; i < attribs.length; i++) {
        var itm = attribs[i];
        ret[itm.name] = itm.value;
      }
      delete elem;
    }

    return ret;
  }
};

$jui.resizer = {
  sizeExists: function (size) {
    return parseFloat(size) > 0;
  }
  , getExtends: function (size, funcs) {
    return {
      juisize: size,
      juirfuncs: funcs || {},
      juiResize: function () {
        try {
          var juirfuncs = this.juirfuncs || {}
          , juisize = this.juisize
              , sizepx = juisize.calcSizePx(this)
              ;

          if (juirfuncs.readypx) {
            sizepx = juirfuncs.readypx({rszr: this, sizepx: sizepx});
          }

          // calculate size in percent
          juisize.calcSizePc(this);
          // set size always in px
          var height = parseInt(sizepx.height)
              , width = parseInt(sizepx.width)
              , style = {};

          if (!$jui.lockHeight)
            style.height = height;
          if (!$jui.lockWidth)
            style.width = width;
          $jui.style.important(this, style, true);

          if (juirfuncs.ready) {
            juirfuncs.ready({rszr: this});
          }

          if (!this.pgpart)
            this.pgpart = this.data('pgpart');
          $jui.sbtoggler.resize(this);

          this.juiResizeChildren();
        } catch (e) {
          $jui.dbgAlert('juiResize()', e.message);
        }
      },
      juiAddChildren: function () {
        var item = {}
        , closest = this.juiclosest
            , target = closest ? closest : juiwindata;
        item[this[0].id] = this;
        if (!target.juiChildren)
          target.juiChildren = {};
        $jq.extend(target.juiChildren, item);

        if (target === juiwindata) {
          if ($jq.contains(juiwindata.pgparts.content[0], this[0])) {
            if (!target.juiChildrenContent)
              target.juiChildrenContent = {};
            $jq.extend(target.juiChildrenContent, item)
          } else if (juiwindata.pgparts.sb_left
              && $jq.contains(juiwindata.pgparts.sb_left[0], this[0])) {
            if (!target.juiChildrenSbLeft)
              target.juiChildrenSbLeft = {};
            $jq.extend(target.juiChildrenSbLeft, item)
          }
        }
      },
      juiResizeChildren: function () {
        if (juiwindata.oninit)
          return;

        var children = this.juiChildren
            , plugin = this.plugin;
        if (!children)
          return;

        for (var id in children) {
          var child = children[id];
          if (plugin == 'juiacc' || plugin == 'juitabs') {
            var cc = $jui.widget.getClosestContent(child)[0] || {}
            , cur = this.curContent ? this.curContent[0] || {} : {};
            if (cc.id === cur.id)
              child.juiResize();
          } else {
            child.juiResize();
          }
        }
      }
    };
  }
  , getSize: function (state, style, fillspace) {
    state = state || {};
    style = fillspace
        ? {height: '100% !important', width: '100% !important'}
    : style || {};
    var height = state.height ? _parseSizeItem(state.height) : false
        , width = state.width ? _parseSizeItem(state.width) : false
        , tmp = 0;

    if (style.height) {
      tmp = _parseSizeItem(style.height);
      if (!height || tmp.important)
        height = tmp;
    }
    if (style.width) {
      tmp = _parseSizeItem(style.width);
      if (!width || tmp.important)
        width = tmp;
    }
    height = height || {percent: true, value: 0.8, src: ''};
    width = width || {percent: true, value: 1.0, src: ''};

    return {
      height: height,
      width: width,
      getHeight: function () {
        var h = this.height;
        if (!h)
          return 0;
        return h.percent ? h.valuePc || 0 : h.valuePx || 0;
      },
      getWidth: function () {
        var w = this.width;
        if (!w)
          return 0;
        return w.percent ? w.valuePc || 0 : w.valuePx || 0;
      },
      getSizeObj: function () {
        return {height: this.height, width: this.width};
      },
      setSizeObj: function (size) {
        this.height = size.height;
        this.width = size.width;
      },
      // call calcSizePx first and then calcSizePc
      calcSizePx: function (rszr) {
        return {height: _calcSizeItemPx('height', rszr),
          width: _calcSizeItemPx('width', rszr)
        };
      },
      calcSizePc: function (rszr) {
        return {height: _calcSizeItemPc(rszr.juisize.height),
          width: _calcSizeItemPc(rszr.juisize.width)
        };
      },
      calcSizeVal: function (height, width, juisize) {
        var size = {height: _getSize(height, juisize.height),
          width: _getSize(width, juisize.width)
        };
        juisize.height.value = size.height;
        juisize.width.value = size.width;
        return size;

        function _getSize(size, target) {
          if (target.percent && target.base > 0) {
            var max = target.pcval_max || 0.99;
            return Math.min(max, (size / target.base).toFixed(4));
          }
          return size;
        }
      }
    };


    function _parseSizeItem(item) {
      if (!item)
        return {};

      var src = item
          , important = false
          , percent = false;

      if (typeof item === 'string') {
        var hitems = item.split(' ');
        item = '' + hitems[0];
        important = hitems[1] ? hitems[1] == '!important' : false;

        if (item.match(/%/)) {
          item = parseFloat(item, 10) / 100; // convert % to decimal
          item = (Math.min(1, Math.max(0, item))).toFixed(4);
        }
      }

      var value = typeof item === 'number' ? item : parseFloat(item);
      percent = value > 0 && value <= 1 ? true : false;

      return {important: important,
        percent: percent,
        value: value,
        src: src
      };
    }

    function _calcBaseItem(type, rszr) {
      var size = rszr.juisize || {}
      , target = size[type];
      if (!target)
        return 0;

      target.base = 0;
      target.baseInner = 0;
      target.pcval_max = 1.0;
      var widget = rszr.juiwidget || rszr
          , cont = rszr.closest('.juicontent');
      if (cont.css('display') != 'none') {
        target.base = (cont.length == 0)
            ? _getSizeMaxPgPart(type, widget)
            : _getSizeMaxContainer(type, cont, widget);
        if ($jui.page.isPreview() && type == 'width' && cont.length == 0) {
          var w = $jq('.preview').width();
          target.base = w > 100 ? w - 4 : target.base - 40;
        }

        var b = widget.border()
            , m = widget.margin()
            , p = widget.padding()
            , diff = (type == 'height')
            ? b.top + b.bottom + m.top + m.bottom + p.top + p.bottom
            : b.left + b.right + m.left + m.right + p.left + p.right
            ;

        target.baseInner = target.base - Math.max(0, diff);
        target.pcval_max = (target.baseInner / target.base).toFixed(4);
      }
      return target.base;
    }

    function _getSizeMaxPgPart(type, widget) {
      if (type != 'height' && type != 'width')
        return 0;
      var pgpart = widget.data('pgpart');
      if (!pgpart)
        return 0;

      var ret = (type == 'height')
          ? $jq(window).height() - pgpart.position().top - 20
          : pgpart.width();
      return parseFloat(ret);
    }

    function _getSizeMaxContainer(type, cont, widget) {
      if (type != 'height' && type != 'width')
        return 0;
      if (!cont || !widget)
        return 0;

      var ret = (type == 'height') ? cont.height() : cont.width();
      return parseFloat(ret - 1);
    }

    function _calcSizeItemPx(type, rszr) {
      if (type != 'height' && type != 'width')
        return 0;

      var size = rszr.juisize || {}
      , target = size[type]
          , widget = rszr.juiwidget || rszr
          , data = rszr.data('resizable') || {}
      , opts = data.options || false;
      if (!target || !widget || !opts)
        return 0;

      var value = target.value;
      _calcBaseItem(type, rszr);
      if (target.percent) {
        value = Math.min(value, target.pcval_max);
        value = value * target.base;
      }

      var min = (type == 'height') ? opts.minHeight : opts.minWidth;
      var max = (type == 'height') ? opts.maxHeight : opts.maxWidth;
      min = min || 10;
      max = max || 10000;
      value = (Math.min(max, Math.max(min, value))).toFixed(2);
      if (type == 'width' && value > target.baseInner) {
        value = target.baseInner;
      }

      target.valuePx = parseFloat(value);
      return target.valuePx;
    }

    function _calcSizeItemPc(st /* SizeTarget */) {
      if (!st)
        return 0;

      var value = (st.valuePx * 100 / st.base).toFixed(2);
      st.valuePc = Math.min(100 * st.pcval_max, value) + '%';
      return st.valuePc;
    }
  }
};

function tb_juiwtoggler(btn, props, edid) {
  btn.toggleClass('pk_jui_marker');
  if ($jui.initialized) {
    var ustate = $jui.get.usrState();
    if (!ustate.edit)
      ustate.edit = {};
    ustate.edit.wizard = btn.hasClass('pk_jui_marker');
    $jui.ajax.storeState('@user@', ustate);
  }
  return false;
}
function tb_juiwizard(btn, props, edid) {
  if ($jq('#juiwtoggler').hasClass('pk_jui_marker')) {
    var idx = $jui.get.actionFromID(props.id)
        , opts = {title: props.title,
          minHeight: 390,
          minWidth: 780,
          maxWidth: 780,
          resizable: false,
          dialogClass: 'juidialog',
          buttons: _getButtons(),
          create: _onCreate,
          open: _onOpen
        }
    ;
    dlgc = $jui.dialogs.get(idx + '-edit-wizard', true, opts);
    if (!dlgc || dlgc.dialog('isOpen'))
      return false;

    dlgc.parent().css({position: 'absolute'});
    dlgc.dialog('open');
  } else {
    insertTags(edid,
        fixtxt(props.open),
        fixtxt(props.close),
        fixtxt(props.sample || '\n???'));
  }
  pickerClose();
  return false;


  function _getButtons() {
    var b = {};
    b[$jui.LANG.btn_apply] = _onApply;
    b[$jui.LANG.btn_preview] = _onPreview;
    b[$jui.LANG.btn_next] = _onNext;
    b[$jui.LANG.btn_back] = _onBack;
    b[$jui.LANG.btn_cancel] = function () {
      $jq(this).dialog('close');
    }
    return b;
  }
  function _onCreate(e, ui) {
    var dlgc = $jq(this)
        , page = '@sys@-' + this.id
        , extra = {caller: 'dialog'};

    dlgc = dlgc.addClass('juicontent').data('self', dlgc);
    $jq.extend(dlgc, {
      jui_isLoading: {}
      , juiIsLoading: function () {
        return !$jui.arr.isEmpty(dlgc.jui_isLoading);
      }
      , juiAjaxPreLoad: function () {
        if (!this.length)
          return;
        dlgc.jui_isLoading[this[0].id] = 1;
      }
      , juiAjaxPostLoad: function () {
        if (!this.length)
          return;
        if (dlgc.jui_isLoading[this[0].id])
          delete dlgc.jui_isLoading[this[0].id];
        if (this.hasClass('juitabs-content')) {
          var fields = $jq(':input:visible:enabled', this);
          if (fields.length) {
            $jq(fields[0]).focus();
          }
        }
      }
    });
    $jui.ajax.parsePageContent(page, dlgc, extra);
  }
  function _onOpen(e, ui) {
    $jq(this).css('max-height', parseInt(juiwindata.clientHeight * 0.90) - 60)
        .parent().css({top: '20px', left: 'auto', right: '20px'});
  }
  function _onApply(e) {
    var dlgc = $jq(this).data('self') || {};
    if (!dlgc.length || dlgc.juiIsLoading())
      return;

    var text = '\n< function _onApply>\n';
    insertTags(edid, '', '', text);
    $jq(this).dialog('close');
  }
  function _onPreview(e) {
    var pvw = _getPreviewEl(this);
    if (!pvw || !pvw.length)
      return;

    var dlgc = $jq(this).data('self') || {};
    if (!dlgc.length || dlgc.juiIsLoading())
      return;

    var extra = {caller: 'dialog'};
    $jui.ajax.parsePageContent("@sys@-dev:tabs:xxx:ccc", pvw, extra);
//        $jui.ajax.parsePageContent("@sys@-dev:tabs:xxx:ccc", pvw);
  }
  function _onNext(e) {
    _move(this, 'next');
  }
  function _onBack(e) {
    _move(this, 'prev');
  }
  function _move(target, action) {
    var dlgc = $jq(target).data('self') || {};
    if (!dlgc.length || dlgc.juiIsLoading())
      return;

    var tabswizard = $jq('.juitabs-wizard', dlgc).data('self') || {}
    , bfuncs = tabswizard.juibfuncs || {};
    if (!bfuncs.bookNaviClick)
      return;

    bfuncs.bookNaviClick(false, action)
  }
  function _getPreviewEl(target) {
    var dlgc = $jq(target).data('self') || {};
    if (!dlgc.length)
      return false;

    var pvw = $jq('.preview', dlgc);
    if (!pvw.length) {
      dlgc.append('<div class="clearer"></div>')
          .append('<div style="margin:10px 0 0;">' + $jui.LANG.btn_preview + '</div>')
          .append('<div class="preview" style="height:300px;width:695px"></div>');
      pvw = $jq('.preview', dlgc);
    }
    return pvw;
  }

}

function juiInitialize(data) {
  try {
    data = data || {};
    $jui = $jui || {};
    juivars = juivars || {};
    $jui.debug = data.debug || false;

    var winvars = juivars.window || {};
    juiwindata = $jq.data(window, 'juivars', winvars);

    _initWin();
    _initSBToggler();
    _initEventBinding();
    _initEdit();
    _initTest();
    delete juivars;
  } catch (e) {
    $jui.dbgAlert('juiInitialize()', e.message);
  } finally {
    $jui.initialized = true;
  }

  //JUITMP::
  function _initTest() {
//            var testlout = $jq('#testlout');
//            if (testlout.length) {
////                alert('testlout.length=' + testlout.length);
//                testlout.layout({ applyDefaultStyles: true });
//            }
  }

  function _initEdit() {
    var juipk = $jq('.picker.juipicker')
        , btns = $jq('button', juipk)
        , juiwt = $jq('#juiwtoggler.toolbutton')
        , width = 0;
    if (!juipk.length || !juiwt.length)
      return;

    var ustate = $jui.get.usrState()
        , edit = ustate.edit || {};
    if (edit.wizard && !juiwt.hasClass('pk_jui_marker')) {
      tb_juiwtoggler(juiwt);
    }

    btns.each(function () {
      width += $jq(this).width() + 4;
    });
    if (width < 60)
      width = 35 * (btns.length - 1) + 20;
    juipk.width(width);
  }

  function _initWin() {
    if ($jui.initialized)
      return;

    var conf = juiwindata.conf || {};
    if (conf.hidepagename) {
      $jq('.header .pagename').addClass('hidden');
    }
    if (conf.insertpageicon) {
      _insertPageIcon();
    }

    juiwindata.clientHeight = $jq(window).height();
    juiwindata.clientWidth = $jq(window).width();

    // init for nested widgets: first outer, then inner
    juiwindata.waiting = {};

    var parts = {};
    switch (juiwindata.pgparts) {
      case 1:
        parts.content = '.right_page';
        parts.sb_left = '.left_sidebar';
        break;
      case 2:
        parts.content = '.left_page';
        parts.sb_right = '.right_sidebar';
        break;
      case 3:
        parts.content = '.center_page';
        parts.sb_left = '.left_sidebar';
        parts.sb_right = '.right_sidebar';
        break;
      default:
        parts.content = '.page';
        break;
    }
    for (var part in parts) {
      parts[part] = $jq(parts[part]);
    }

    juiwindata.pgparts = parts;
    juiwindata.hasData = true;


    function _insertPageIcon() {
      var bch = $jq('#bar__top').children()[0];
      if (!bch)
        return;

      $jq(bch).before('<a id="logo_image_left" href="'
          + DOKU_BASE + 'doku.php" title="start"></a>');
    }
  }

  function _initEventBinding() {
    $jq(window).resize($jui.win.onResize);
    $jq(window).keydown($jui.win.onKeydown);
    $jq("body").delegate("*", "click", $jui.win.delegateBodyClick);
    $jq("body").delegate("a", "click", $jui.win.delegateAClick);
  }

  function _initSBToggler() {
    try {
      var conf = juiwindata.conf || {}
      , juit = conf.juitoggler || {}
      , parts = juiwindata.pgparts
          , create = parts.sb_left ? juit.enabled : false
          , lockable = juit.lockable || false;
      if (create) {
        var data = {sidebar: parts.sb_left
          , content: parts.content
          , lockable: lockable
          , pinable: juit.pinable
        };
        $jui.sbtoggler.create(data);
        _restoreSbtState();
        _initSbtEventBinding();
      }
    } catch (e) {
      $jui.dbgAlert('_initSBToggler', e.message);
    }
    return;

    function _restoreSbtState() {
      var sbt = $jui.sbtoggler
          , pgstate = $jui.get.pgState();
      if (!pgstate.locked)
        return;

      //restore sidebar-toggler-state
      $jui.flags.keyCtrl = true;
      pgstate.locked--;
      sbt.lockPage(true);
      $jui.flags.keyCtrl = false;
      if (!pgstate.pinned)
        sbt.pinSidebar(false);
      if (pgstate.hidden)
        sbt.hideSidebar(true);
    }

    function _initSbtEventBinding() {
      $jq('*').bind('keydown', function (e) {
        if (e.which == 17 && !$jui.flags.keyCtrl) {
          $jui.sbtoggler.setCtrlFlag(true);
        }
      });
      $jq('*').bind('keyup', function (e) {
        if (e.which == 17 && $jui.flags.keyCtrl) {
          $jui.sbtoggler.setCtrlFlag(false);
        }
      });
    }
  }

}

// main-entry-point, called from html-page (head-section)
// please, not on top of that scriptfile
function juiOnJqReady(plugin, data) {
  $jui.onJqReady(plugin, data);
}

// EOF


