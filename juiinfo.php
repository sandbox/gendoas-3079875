<?php

if (!defined('JUI_PATH_DELIMITER')) define('JUI_PATH_DELIMITER', "§");

/**
 * Description of JuiInfo
 *
 * @author Albin
 */
class JuiInfo {

    private $parts      = array();
    private $action     = '';
    private $is_syspage = false;
    private $pages      = array();
    private $widgets    = array();

    private $curPart    = false;
    private $curPage    = false;
    private $curWidget  = false;

    public function &getCurWidget() {
        return $this->curWidget;
    }
    public function setCurWidget($juiid) {
        $ret = false;
        if ($this->curWidget) {
            $ret = $this->curWidget['id'];   // return old curWidget-id
            unset($this->curWidget);
        }

        if (!empty($juiid)) $this->curWidget =& $this->widgets[$juiid];
        if (empty($this->curWidget)) $this->curWidget = false;
        return $ret;
    }

    public function __construct($parts, $action) {
        $this->action = $action;
        foreach ($parts as $part) {
            $this->parts[$part] = array('id'        => $part,
                                        'type'      => 'part',
                                        'multiple'  => array(),
                                        'rendered'  => false);
        }
    }

    public function getAction() { return $this->action; }
    public function isPreview() { return ($this->action=='preview'); }
    public function isSyspage() { return $this->is_syspage; }
    public function setIsSyspage() { $this->is_syspage = true; }
    public function &getParts() { return $this->parts; }
    public function &getPages() { return $this->pages; }
    public function &getWidgets() { return $this->widgets; }

    public function getWidgetListMultiple($pid) {
        $widgets = $this->widgets;
        if (empty($widgets)) {
            return false;
        }


        $ret = array();
        if (juiGetIsAjaxCall()) {
            // for ajax-call each widget gets a new unique id
            foreach ($widgets as $widget) {
                $ret[] = $widget['id'];
            }
        } else {
            foreach ($widgets as $widget) {
                if ($widget['occurrence'] > 1 && !empty($widget['parts'])) {
                    foreach ($widget['parts'] as $part) {
                        if ($part == $pid) {
                            $ret[] = $widget['id'];
                            break;
                        }
                    }
                }
            }
        }

        return !empty($ret) ? $ret : false;
    }

    public function &getCurPart() { return $this->curPart; }
    public function &getCurPage() { return $this->curPage; }
    
    public function getIsAllRendered() {
        foreach ($this->parts as $part ) {
            if (!$part['rendered']) return false;
        }
        return true;
    }

    public function resetCurData() {
        unset($curPart);
        $curPart = false;
        unset($curPage);
        $curPage = false;

        if (!empty($this->pages)) {
                        $pages = $this->pages;
            foreach ($this->pages as &$page)  {
               unset($page['parent']);
               $xx = 0;
            }
        }
        if (!empty($this->$widgets)) {
            foreach ($this->widgets as &$widget)  {
               unset($widget['parent']);
            }
        }
    }
    
    /**
     * Set current part (content, left_sidebar, right_sidebar)
     *
     * @param <string> $part
     * @return reference<array> if set, false else
     */
    public function &setCurPart($part) {
        // unset reference first !!!
        unset($this->curPart);
        $this->curPart = false;
        
        if (!isset($this->parts[$part])) {
            return false;
        }
        
        $this->curPart =& $this->parts[$part];
        return $this->curPart;
    }

    /**
     *
     * @param <string> $raw_id
     * @param <bool> $create
     * @return reference<array> if exists, false else
     */
    public function &getPage($raw_id, $create=false) {
        // return existing page
        if ( isset($this->pages[$raw_id]) ) {
           return $this->pages[$raw_id];
        }

        // do not create: return false
        if (!$create) {
            return false;
        }

        // create and return new page
        $juivars = p_get_metadata($raw_id, 'juivars');
        if (!is_array($juivars)) $juivars = false;
        $this->pages[$raw_id] = array( 'id'             => $raw_id,
                                       'type'           => 'page',
                                       'juivars'        => $juivars,
                                       'rebuild'        => false,
                                       'occurrence'     => 0);
        return $this->pages[$raw_id];       //JUIFIX:: getPage... $page['rebuild'] 
    }

    public function &setCurPage($raw_id) {
        // unset reference first !!!
        unset($this->curPage);
        $this->curPage = false;

        if ($raw_id === false) {
            // there is no current page
            return true;
        }

        // set existing page only
        $page =& $this->getPage($raw_id);
        if (!$page) {
            return false;
        }

        $this->curPage =& $page;
        return $this->curPage;
    }



    public function &addPage(&$page, &$parent=false, $setCurrent=false) {
        
        if (!$page) return false;
        if ($parent && $this->_isRefIdentical($page, $parent)) {
            // page and parent can't be identical
            return false;
        }

        // unset current and set on success again
        if ($setCurrent) {
            // unset reference first !!!
            unset($this->curPage);
            $this->curPage = false;
        }

        if ($parent) {
            $pid = $parent['id'];
            $type = $parent['type'];
            if ( !isset($pid) || !isset($type) ) {
                return false;
            }
            // accept only a part or a page
            if ( $type!='part' && $type!='page' ) {
                return false;
            }
            // accept only in this class created part
            if ( $type == 'part'
             && !$this->_isRefIdentical($parent, $this->parts[$pid]) ) {
                return false;
            }
            // accept only in this class created page
            if ( $type == 'page'
             && !$this->_isRefIdentical($parent, $this->pages[$pid]) ) {
                return false;
            }
        }

        // accept only in this class created page
        $raw_id = $page['id'];
        if ( !isset($raw_id) 
          || !$this->_isRefIdentical($page, $this->pages[$raw_id]) ) {
            return false;
        }

        if (!$parent) $parent =& $this->curPart;
        if (!$parent || $this->isPageNesting($parent, $raw_id)) {
            return false;
        }

        // add to parent and increment occurrency
        $parent['pages'][$raw_id] =& $page;
        ++$page['occurrence'];

        // $page['parent'] shouldn't be empty
        if (empty($page['parent'])) {
            $page['parent'] =& $parent;
        }
        //enter cur-parent and parents, avoid loop
        $path = $this->_getPagePath($page);
        if (is_array($path) && !in_array($page['id'], $path)) {
            $page['parent'] =& $parent;
            if (!empty($path[0])) {
                if (empty($page['parts']) ||
                    !in_array($path[0], $page['parts'])) {
                    $page['parts'][] = $path[0];
                }
            }
//            $path[] = $page['id'];
//            $page['pathes'][] = implode(JUI_PATH_DELIMITER, $path);
        }
        // set current if required
        if ($setCurrent) $this->curPage =& $page;
        
        return $page;
        
    }

    public function isPageNesting($page, $raw_id) {
        if (empty($page) || $page['type'] != 'page') {
            return false;
        }

        $path = $this->_getPagePath($page);
        if (!is_array($path)) {
            return false;
        }
        return in_array($raw_id, $path) ? true : false;
    }


    public function &getWidget($uid, $create=false) {
        // return existing widget
        if ( isset($this->widgets[$uid]) && $this->widgets[$uid]['id']==$uid ) {
           return $this->widgets[$uid];
        }

        // do not create: return false
        if (!$create) {
            return false;
        }

        // create and return new widget
        $this->widgets[$uid] = array( 'id'             => $uid,
                                      'type'           => 'widget',
                                      'occurrence'     => 0);
        return $this->widgets[$uid];
    }


    public function &addWidget(&$widget, &$page=false, $occurrence=1) {

        if (!$page) {
            $page =& $this->getCurPage();
            if (!$page) return false;
        }

        $pid = $page['id'];
        $type = $page['type'];
        // accept only a page
        if ( !isset($pid) || $type != 'page' ) {
            return false;
        }
        // accept only in this class created page
        if ( !$this->_isRefIdentical($page, $this->pages[$pid]) ) {
            return false;
        }

        // accept only in this class created widget
        $wid = $widget['id'];
        if ( !isset($wid)
          || !$this->_isRefIdentical($widget, $this->widgets[$wid]) ) {
            return false;
        }

        // add to parent and increment occurrency
        $page['widgets'][$wid] =& $widget;
        $widget['occurrence'] += $occurrence;
        $widget['parent'] =& $page;

        // $widget['parent'] shouldn't be empty
        if (empty($widget['parent'])) {
            $widget['parent'] =& $page;
        }
        //enter cur-parent and parents, avoid loop
        $path = $this->_getWidgetPath($widget);
        if (is_array($path) && !in_array($widget['id'], $path)) {
            $widget['parent'] =& $page;
            if (!empty($path[0])) {
                if (empty($widget['parts']) ||
                    !in_array($path[0], $widget['parts'])) {
                    $widget['parts'][] = $path[0];
                }
            }
//            $path[] = $widget['id'];
//            $widget['pathes'][] = implode(JUI_PATH_DELIMITER, $path);
        }

        return $page;
        
    }

    public function addWidgetPageId(&$widget, $pageid) {
        if (!is_array($widget['pages'])) {
            $widget['pages'] = array($pageid);
        } elseif (!in_array($pageid, $widget['pages'])) {
            $widget['pages'][] = $pageid;
        }
    }

    public function getJuiVarsPage($raw_id) {

        $page =& $this->getPage($raw_id);

        if (!$page['rebuild'] && $page['juivars']) {
            // already parsed
            return $page['juivars'];
        }

        if (empty($page['widgets'])) {
            // no widgets on that page
            $page['juivars'] = false;
            return false;
        }

        $widgetsExists = array();
        $widgetsSubpages = array();
        $jsvars = array();
        $depends = array();

        // parse widgets
        foreach ($page['widgets'] as $widget) {
            $plugin = $widget['plugin'];
            if (array_key_exists($widget['id'], $jsvars)) continue;  // already done
            
            // gather existent plugins (juiacc, juitabs, ...)
            $widgetsExists[$widget['plugin']] = true;
            //
            if ( !empty($widget['subpages']) ) {
                $widgetsSubpages[$widget['id']] = $widget['subpages'];

                if ($this->ensurePageJuiVars($page)) {      //$page['juivars']
                    $this->includePageWidgets($page);
                }
            }

            // jsvars
            $wid = $widget['id'];
            $jsvars[$wid] = $widget['jsvars'];
            $jsvars[$wid]['occurrence'] = $widget['occurrence'];

            // depends
            if ( !empty($widget['depends']) ) {
                foreach ($widget['depends'] as $file) {
                    if (in_array($file, $depends)) continue;
                    $depends[] = $file;
                }
            }
        }

        $jui = array();
        if (!empty($widgetsExists)) $jui['widgetsExists'] = $widgetsExists;
        if (!empty($widgetsSubpages)) $jui['widgetsSubpages'] = $widgetsSubpages;
        if (!empty($jsvars)) $jui['jsvars'] = $jsvars;
        if (!empty($depends)) $jui['depends'] = $depends;

        $page['juivars'] = !empty($jui) ? $jui : false;
        return $page['juivars'];
    }

    public function ensurePageJuiVars(&$page) {
                return true;
//        if (isset($page['juivars'])) {
//            return is_array($page['juivars']);
//        }
//
//        $juivars = p_get_metadata($page['id'], 'juivars');
//        $page['juivars'] = false;       // is set now
//        if (is_array($juivars)) $page['juivars'] = $juivars;
//        return is_array($page['juivars']);
    }

    public function includePageWidgets(&$page, $occurrence=1) {

        if (!$page) return;     //JUIFIX:: includePageWidgets
        $jsvars = $page['juivars']['jsvars'];
        if (!is_array($jsvars)) return;

        foreach ($jsvars as $id => $vars) {
            $widget =& $this->getWidget($id, true);
            $widget['jsvars'] = $vars;
            // add the widget to the page
            if ($occurrence < $vars['occurrence']) $occurrence = $vars['occurrence'];
            $this->addWidget($widget, $page, $occurrence);

            $widgetsSubpages = $page['juivars']['widgetsSubpages'];
            if (is_array($widgetsSubpages)) {
                foreach ($widgetsSubpages as $wid => $pages) {
                    if (is_array($pages)) {
                        foreach ($pages as $pid) {
                            list($pid, $multi) = explode('&', $pid, 2);

                            $pg =& $this->getPage($pid, true);
                            $pg =& $this->addPage($pg, $page);
                            if (!$pg) continue;

                            if ($this->ensurePageJuiVars($pg)) {
                                // nesting pages,
                                // FIXME: handled only inside of juiwidgets
                                $occurrence = 1;
                                if ($multi=='multi') $occurrence = 2;
                                $this->includePageWidgets($pg, $occurrence);
                            }
                        }
                    }
                }
            }
        }

    }





    private function _isRefIdentical(&$a, &$b) {
           if (!is_array($a) || !is_array($b)) return false;
           $uid = uniqid("");
           $a[$uid] = true;
           $bResult = !empty($b[$uid]);
           unset($a[$uid]);
           return $bResult;
    }


    private function _getPagePath($page) {
        $ret = array();
        $next =& $page['parent'];
        while ($next['type'] == 'page') {
            if ( in_array($next['id'], $ret) ) {
                return false;
            }
            array_unshift($ret, $next['id']);
            $next =& $next['parent'];
        }

        // part required
        if ($next['type'] !== 'part') {
            return 'false';
        }

        array_unshift($ret, $next['id']);
        return $ret;
    }

    private function _getWidgetPath($widget) {
        $ret = array();
        $next =& $widget['parent'];
        while ($next['type'] == 'widget') {
            if ( in_array($next['id'], $ret) ) {
                return false;
            }
            array_unshift($ret, $next['id']);
            $next =& $next['parent'];
        }

        // page required
        if ($next['type'] !== 'page') {
            return 'false';
        }
        $pgid = $next['id'];
        while ($next['type'] == 'page') {
            array_unshift($ret, $next['id']);
            $next =& $next['parent'];
            if ($pgid == $next['id']) return false; // avoid loop
        }

        // part required
        if ($next['type'] !== 'part') {
            return 'false';
        }

        array_unshift($ret, $next['id']);
        return $ret;
    }

}
?>
