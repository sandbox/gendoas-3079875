<?php
/**
 * AJAX call handler for JUI-Widgets
 *
 * @license     GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');


class juiajax {
    private $juidata;
    
    // constructor
    public function __construct() {
        $this->juidata = json_decode( $_REQUEST['juidata'], true );
        $this->_initialize($this->juidata);
    }
    
    private function _initialize($juidata) {

        // make flag global
        juiSetIsAjaxCall();

        // isNesting: prevent nesting jui-widgets
        if (isset($juidata['isNesting'])) {
            foreach ($juidata['isNesting'] as $plugin) {
                juiSetIsNesting($plugin, true);
            }
        }

        // set global jui-id counter
        JuiCounter::uidReset($juidata['uidLast']);

        // initialize jui-widgets
        $foo = false;
        trigger_event('TPL_METAHEADER_OUTPUT', $foo);
    }
    
    public function parseContent() {
        $content = $this->juidata['content'];
        if (!isset($content)) {
            echo '<div>Error AjaxRequest: content not found</div>';
            return;
        }
        
        
        $content = str_replace('&quot;', '"', strtolower($content));
        list($ar, $foo) = explode('>', $content, 2);
        list($key, $opts) = explode(' ', $ar, 2);
        if ( $key == '<juiajax' ) {
            $allowed = '*';     // FIXME:: what is allowed for ajax call  ??
            $opts = trim($opts, '/');
            $data['options'] = juiGetOptionsArray($opts, $allowed);
            if ( isset($data['options']['call']) ) {
                $callfn = '_parse_' . trim($data['options']['call']);
                if (method_exists($this, $callfn)) {
                    // 
                    // handle AjaxCall now
                    //
                    $this->$callfn($data);
                    // ready handled
                    return;
                }
            }
        }
        
        // AjaxRequest not handled
        echo '<div>Error AjaxRequest: not handled:</div>';
        echo '<div>'.htmlspecialchars($content).'</div>';
    }

    // 
    private function _parse_page($data){
      global $conf;
      global $INFO;
      global $QUERY;
      global $ID;
      global $NS;
      global $ACT;
      global $juiINFO;

        // now first resolve and clean up the $id
        $cpage = $this->juidata['jsvars']['page'];
        $namespace = isset($cpage) ? getNS($cpage) : $this->juidata['JSINFO']['namespace'];
        $id = $data['options']['page'];
        
        // sys-page ?
        if (preg_match('/^:*@sys@-/', $id, $matches)) {
            $this->_parseSyspage($id);
            return; // sys-page done
        }
        
        
        
        // wiki-page
        resolve_pageid($namespace,$id,$foo);
        juiSetAjaxData(array('pageid' => $id));
        
        $_REQUEST['id'] = $id;
        $QUERY = trim($_REQUEST['id']);
        $ID    = getID();
        $NS    = getNS($ID);
        //make infos about the selected page available
        $INFO = pageinfo();
        // no section-edit
        $INFO['writable'] = false;

        if ( !$INFO['exists'] ) {
            // file doesn't exist
            echo $this->_parseNewpage(array('id'=>$ID, 'INFO'=>$INFO));
            return;
        }

        $ACT = 'show';
        if ( !tpl_content(false) ) {
            echo "<div>Error: NoContent</div>";
        }
    }

    private function _parseNewpage($data) {
        $newpage = juiGetRawLacale('newpage');
        $ret = str_replace('@PAGEID@', $data['id'], $newpage);
        $ret = p_render('xhtml', p_get_instructions($ret), $data['INFO']);
        // add juivars
        $jsvars = array('ajaxData' => juiGetAjaxData());
        $ret   .= DOKU_LF . '<script type="text/javascript" charset="utf-8"><!--//--><![CDATA[//><!--'
                    . DOKU_LF . "juivars=" . json_encode($jsvars) . ';'
                    . DOKU_LF . '//--><!]]></script>' . DOKU_LF;
        
        return $ret;
    }
    
    private function _parseSyspage($syspage){
        require_once DOKU_PLUGIN_JUI . 'juisyscall.php';
        if (class_exists('juisyscall')) {
            $GLOBALS['juiINFO']['syscall']['extra'] = $this->juidata['extra'];
            $syscall = new juisyscall();
            if ($syscall->parse('page', $syspage)) {
                $html_output = $syscall->getHtmlOutput();
                // add juivars and print output
                trigger_event('JUI_SYSPAGE_DISPLAY',$html_output,'ptln');
            } else { echo($syscall->getHtmlError()); }
            return true;
        }
        echo('<div class="info">Error Syspage ' . $id . '</div>');
        return false;
    }
    
    private function _parse_form($data){
        require_once DOKU_PLUGIN_JUI . 'juisyscall.php';
        // sysform only
        $sysform = $data['options']['form'];
        if (class_exists('juisyscall')) {
            $syscall = new juisyscall();
            if ($syscall->parse('form', $sysform)) {
                $html_output = $syscall->getHtmlOutput();
                echo $syscall->getHtmlOutput();
            } else {echo $syscall->getHtmlError();}
            return true;
        }
        $out = '<div>Da sind wir also: _parse_form()<br/>'.$sysform.'</div>';
        echo $out;
    }
    
    
}
