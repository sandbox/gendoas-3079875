<?php

/**
 * DokuWiki Plugin juihelper (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin.spreizer@web.de>
 */
// must be run within Dokuwiki
if (!defined('DOKU_INC'))
  die();

if (!defined('DOKU_LF'))
  define('DOKU_LF', "\n");
if (!defined('DOKU_TAB'))
  define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN'))
  define('DOKU_PLUGIN', DOKU_INC . 'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI'))
  define('DOKU_PLUGIN_JUI', DOKU_PLUGIN . 'juiwidget/');
if (!defined('TXT_CONTENT'))
  define('TXT_CONTENT', 'content');
if (!defined('TXT_SIDEBAR_LEFT'))
  define('TXT_SIDEBAR_LEFT', 'left_sidebar');
if (!defined('TXT_SIDEBAR_RIGHT'))
  define('TXT_SIDEBAR_RIGHT', 'right_sidebar');

require_once DOKU_PLUGIN . 'action.php';
require_once DOKU_PLUGIN_JUI . 'juicounter.php';
require_once DOKU_PLUGIN_JUI . 'juiinfo.php';
require_once DOKU_PLUGIN_JUI . 'juiutils.php';
require_once DOKU_PLUGIN_JUI . 'juischemes.php';
require_once(DOKU_PLUGIN_JUI . 'juidata_io.php');

class action_plugin_juiwidget extends DokuWiki_Action_Plugin {

  private $initialized = false;

  public function register(Doku_Event_Handler $controller) {
    $controller->register_hook('JUI_DEBUG_BEFORE', 'BEFORE', $this, 'handleDebugBefore');
    $controller->register_hook('JUI_DEBUG_AFTER', 'BEFORE', $this, 'handleDebugAfter');

    $controller->register_hook('PARSER_METADATA_RENDER', 'AFTER', $this, 'handleMetadataRenderAfter');
    $controller->register_hook('TPL_METAHEADER_OUTPUT', 'BEFORE', $this, 'handleMetaheaderOutputBefore');
    $controller->register_hook('TPL_CONTENT_DISPLAY', 'BEFORE', $this, 'handleContentDisplayBefore');
    $controller->register_hook('IO_WIKIPAGE_WRITE', 'BEFORE', $this, 'handleWikipageWrite');
    $controller->register_hook('PARSER_CACHE_USE', 'BEFORE', $this, 'handleParserCacheBefore');
    $controller->register_hook('PARSER_CACHE_USE', 'AFTER', $this, 'handleParserCacheAfter');
    $controller->register_hook('PLUGIN_INCLUDE_RENDERED', 'AFTER', $this, 'handlePluginIncludeRendered');
    $controller->register_hook('TOOLBAR_DEFINE', 'AFTER', $this, 'handleToolbarDefineAfter');
    $controller->register_hook('AJAX_CALL_UNKNOWN', 'BEFORE', $this, 'handleAjaxCallUnknown');

    $template = $GLOBALS['conf']['template'];
    if ($template == 'arctic' || $template == 'arctic-mbo') {
      $this->sidebar_exists = true;
      $controller->register_hook('TPL_ARCTIC_RENDER', 'BEFORE', $this, 'handleSidebarRenderBefore');
      $controller->register_hook('TPL_ARCTIC_RENDER_SB', 'AFTER', $this, 'handleSidebarRenderItemAfter');
      $controller->register_hook('TPL_ARCTIC_DISPLAY', 'BEFORE', $this, 'handleSidebarDisplayBefore');
    }


    $controller->register_hook('JUI_SYSPAGE_DISPLAY', 'BEFORE', $this, 'handleSyspageDisplayBefore');
  }

  public function isPreview() {
    static $hasValue = false;
    static $isPreview = false;
    if (!$hasValue) {
      $isPreview = juiGetJuiInfo()->isPreview();
      $hasValue = true;
    }
    return $isPreview;
  }

  public function handleToolbarDefineAfter(Doku_Event &$event, $param) {
    global $lang;

    $plugins = juiGetJuiPlugins();
    if (empty($plugins))
      return;

    $images = DOKU_BASE . 'lib/plugins/juiwidget/images/';
    $list = array();
    foreach ($plugins as $plugin) {
      $data = $this->_retrivePluginSample($plugin);
      $list[] = array(
          'id' => "juiw-$plugin",
          'type' => 'juiwizard',
          'title' => $this->getLang("{$plugin}_title"),
          'icon' => $images . "$plugin.png",
          'open' => $data['open'],
          'close' => $data['close'],
          'sample' => $data['sample'],
      );
    }

    $list[] = array(
        'id' => 'juiwtoggler',
        'type' => 'juiwtoggler',
        'title' => 'tb_juiwtoggler',
        'icon' => $images . 'juiwizard.png',
        'key' => "0",
    );

    $juipicker = array(
        'type' => 'picker',
        'title' => $this->getLang('jui_widgets'),
        'icon' => $images . 'juipicker.png',
        'class' => 'juipicker',
        'list' => $list,
        'block' => true
    );
    $event->data[] = $juipicker;
  }

  private function _retrivePluginSample($plugin = 'fault', $file = 'default') {
    $ret = array();
    $more = '';
    switch ($plugin) {
      case 'juiacc':
//                $more = 'scheme="compact:crl"';
        $more = 'scheme="compact:crl"';
        $ret['sample'] = '\n  <acc-section>Text...</acc-section>';
        break;
      case 'juilayout':
//                $more = 'scheme="compact:rl"';
        $more = 'scheme="fillspace:r"';
        $ret['sample'] = '\n  <layout-section pane="center">Text...</layout-section>';
        break;
      case 'juitabs':
//                $more = 'scheme="compact:crl"';
        $more = 'scheme="compact:bf" addFlags="nonavi"';
        $ret['sample'] = '\n  <tab-section>Text...</tab-section>';
        break;
      default:
        $ret['sample'] = '**fault**';
        break;
    }

    $ret['open'] = "\\n<$plugin $more>\\n";
    $ret['close'] = "\\n</$plugin>\\n";
    $s = @file_get_contents(DOKU_PLUGIN . "{$plugin}/samples/{$file}.txt");
    if ($s)
      $ret['sample'] = str_replace("\n", '\n', $s);

    return $ret;
  }

  public function handleAjaxCallUnknown(Doku_Event &$event, $param) {
    list($key, $func) = explode('_', $event->data, 2);
    if ($key !== 'juiajax')
      return;

    $allowed = array('parseContent');
    if (!in_array($func, $allowed))
      return;

    require_once DOKU_PLUGIN_JUI . 'ajax.php';
    $juiajax = new juiajax();
    if (!$juiajax || !method_exists($juiajax, $func)) {
      return;
    }

    // function-call
    $juiajax->$func();

    // prevent default 
    $event->preventDefault();
    $event->stopPropagation();
//    $event->_default = false;
  }

  public function handleDebugBefore(Doku_Event &$event, $param) {
    static $no = 0;
    static $part = false;
    static $events = array();
    global $ID;
    return;

    $name = $event->data->name;
    $data = $event->data->data;

    if ($this->initialized && $part != juiInfoGetCurPartId()) {
      $no = 0;
      $part = juiInfoGetCurPartId();
    }


//        if ($part != TXT_SIDEBAR_LEFT) return;
//        if ($part != TXT_CONTENT) return;


    $debug = array('',
//                                        'TPL_ARCTIC_RENDER',
//                                        'TPL_ARCTIC_RENDER_SB',
//                                        'TPL_ACT_RENDER',
//                                        'PARSER_CACHE_USE',
        'PARSER_METADATA_RENDER',
//                                        'IO_WIKIPAGE_READ',
//                                        'PARSER_WIKITEXT_PREPROCESS',
//                                        'PARSER_HANDLER_DONE',
//                                        'RENDERER_CONTENT_POSTPROCESS',
//                                        'HTML_SECEDIT_BUTTON',
//                                        'TPL_TOC_RENDER',
//                                        'TPL_CONTENT_DISPLAY',
//                                        ''
    );
    if (!juiGetIsAjaxCall()) {
//                            return;
    }
    $debug = '*';
    if (is_array($debug) && !in_array($name, $debug)) {
      return;
    } else {
      $events[$name] = $name;
    }


    $lvl = 0;
    $more = '';
    switch ($name) {
      case 'PARSER_CACHE_USE':
        if (empty($data->page)) {
          $pos = strpos($data->file, '/pages/');
          $more = '...' . substr($data->file, $pos);
          $xx = 0;
        } else {
          $more = "page=$data->page, mode=$data->mode";
        }
        break;
      case 'PARSER_WIKITEXT_PREPROCESS':
        break;
      case 'PARSER_METADATA_RENDER':
        $lvl = -1;
        break;
      case 'TPL_CONTENT_DISPLAY':
        $xx = implode("',\n'", array_keys($events));
        break;
      case 'RENDERER_CONTENT_POSTPROCESS':
        $mode = $data[0];
        $more = "mode=$mode; ID=$ID";
        if ($mode == 'xhtml')
          $html = $data[1];
        break;
      case RENDERER_INCLUDE_POSTPROCESS:
        $more = "ID=$ID,  included=$data[0]";
        $lvl = -1;
        break;
    }

    if (true) {
      $no++;
      if (!empty($more))
        $more = ":: $more";
      $msg = "$no: $name$more";
      msg($msg, $lvl);
    }
  }

  public function handleDebugAfter(Doku_Event &$event, $param) {
    static $no = 0;
    static $part = false;

    global $ID;
    return;

    if ($this->initialized && $part != juiInfoGetCurPartId()) {
      $no = 0;
      $part = juiInfoGetCurPartId();
    }

//        if ($part != TXT_SIDEBAR_LEFT) return;
//        if ($part != TXT_CONTENT) return;

    $name = $event->data['name'];
    $data = $event->data['data'];

    $debug = array('',
//                                        'TPL_ARCTIC_RENDER',
//                                        'TPL_ARCTIC_RENDER_SB',
//                                        'TPL_ACT_RENDER',
//                                        'PARSER_CACHE_USE',
        'PARSER_METADATA_RENDER',
//                                        'IO_WIKIPAGE_READ',
//                                        'PARSER_WIKITEXT_PREPROCESS',
//                                        'PARSER_HANDLER_DONE',
//                                        'RENDERER_CONTENT_POSTPROCESS',
//                                        'HTML_SECEDIT_BUTTON',
//                                        'TPL_TOC_RENDER',
//                                        'TPL_CONTENT_DISPLAY',
//                                        ''
    );
//                        return;
    $debug = '*';
    if (is_array($debug) && !in_array($name, $debug)) {
      return;
    }

    $lvl = 0;
    $more = '';
    switch ($name) {
      case 'PARSER_CACHE_USE':
        $pos = strpos($data->cache, '/cache/');
        $more = '...' . substr($data->cache, $pos);
        break;
      case 'PARSER_METADATA_RENDER':
        $lvl = -1;
        break;
      case 'RENDERER_CONTENT_POSTPROCESS':
        $mode = $data[0];
        $more = "mode=$mode; ID=$ID";
        if ($mode == 'xhtml')
          $html = $data[1];
        break;
    }

    if (true) {
      $no++;
      if (!empty($more))
        $more = ":: $more";
      $msg = "<__after__> $no: $name$more";
      msg($msg, $lvl);
    }
  }

  public function handleMetadataRenderAfter(Doku_Event &$event, $param) {
    global $ID;

    $jui = juiGetJuiInfo()->getJuiVarsPage($ID);
    if ($jui) {
      // append juiwidget-metadata for storing in mata-file
      $event->result['current']['juivars'] = $jui;
    }
  }

  public function handleMetaheaderOutputBefore(Doku_Event &$event, $param) {
    // ensure initializing
    $this->_juiInitialize();
    // append js 
    $this->_juiAppendDataScript($event, $param);
  }

  private function _juiInitialize() {
    global $conf, $juiINFO;

    if ($this->initialized)
      return true;

    // prepare jui data directory
    $juidatadir = fullpath($conf['savedir'] . '/' . $this->getConf('juidatadir'));
    if (!@file_exists($juidatadir))
      io_mkdir_p($juidatadir);
    $conf['juidatadir'] = init_path($juidatadir);

    // create juiinfo-object
    $juiINFO['JuiInfo'] = & $this->_createJuiInfo();
    if (!juiInfoSetCurPart(TXT_CONTENT)) {
      juiDbgMsg('JuiError: Couldn\'t set part "' . TXT_CONTENT . '" (_juiInitialize)', -1);
      return false;
    }

    $this->initialized = true;
    return true;
  }

  private function _juiAppendDataScript(Doku_Event &$event, $param) {
    global $conf, $INFO;

    $morecode = array();
    $addjs = '';
    trigger_event('JUIWIDGET_READY', $morecode, NULL, false);
    foreach ($morecode as $id => $mc) {
      $addjs .= '// BEGIN --- ' . $id . PHP_EOL;
      $addjs .= $mc . PHP_EOL;
      $addjs .= '// END --- ' . $id . PHP_EOL;
    }



    $event->data['script'][] = array(
        'type' => 'text/javascript',
        'charset' => 'utf-8',
        '_data' => 'var juivars = null;' . PHP_EOL,
    );
  }

  public function handlePluginIncludeRendered(Doku_Event &$event, $param) {
    global $ID;

    $data = & $event->data;
    $innerID = $data['innerid'];
    $outerID = $data['outerid'];

    if ($innerID == $ID)
      return;


    $testVars = $this->_testVars();

    // - determine if outer page ($ID) has to be rendered again
    // - reset current page to outer page
    $purge = false;

    $juiInfo = juiGetJuiInfo();
    $pageInner = & $juiInfo->getCurPage();
    if ($innerID == $pageInner['id'] && $juiInfo->ensurePageJuiVars($pageInner)) {
      if ($pageInner['juivars']['purge'] === true) {
        // pass purge to outer page
        $purge = true;
      }

      // pageInner <is-element-of> $widget <is-element-of> pageOuter:
      // prevent nesting the same type of widget ('plugin')
      if (!$purge && ($widget = & $juiInfo->getCurWidget())) {
        $plugin = $widget['plugin'];
        $widgetsExists = $pageInner['juivars']['widgetsExists'];
        if ($widgetsExists[$plugin] === true) {
          $purge = true;
        }
      }
    }

    $pageOuter = & $juiInfo->setCurPage($outerID);
    if (!$pageOuter) {
      juiDbgMsg("JuiError setCurPage('$outerID') - handlePluginIncludeRendered()", -1);
      return false;
    }

    if ($purge && $pageOuter['juivars']['purge'] !== true) {
      $pageOuter['juivars']['purge'] = true;
      p_set_metadata($outerID, array('juivars' => $pageOuter['juivars']));
    }


    // for debugging only
    if ($GLOBALS['INFO']['isadmin'] && $GLOBALS['conf']['allowdebug']) {
      // is nesting correct - helper
      $html = & $data[1];
      $pd = $this->_getPageTags($innerID);
      $html = $pd[0] . $html . $pd[1];
    }
    return true;
  }

  /**
   * On entering rendering sidebar
   *
   * @param Doku_Event $event
   * @param <type> $param 
   */
  public function handleSidebarRenderBefore(Doku_Event &$event, $param) {
    // enter rendering sidebar.

    juiGetJuiInfo()->resetCurData();    // reset data for sidebar-part
    juiInitAjaxReplace();               // empty replace-array
    // set the current part
    $part = $event->data == 'left' ? TXT_SIDEBAR_LEFT : TXT_SIDEBAR_RIGHT;
    juiInfoSetCurPart($part);
  }

  public function handleSidebarRenderItemAfter(Doku_Event &$event, $param) {
    // sidebar item  is ready (main, user, group, ...),
    // reset current page to false
    juiGetJuiInfo()->setCurPage(false);
  }

  public function handleSyspageDisplayBefore(Doku_Event &$event, $param) {
    // final html-preparation
    $this->_htmlFinalRevise($event->data);
    // append variables for JavaScript
    $jsvars = $this->_getJsVarsFinal();
    $event->data .= $this->_getJsVarsHtml($jsvars);
  }

  public function handleSidebarDisplayBefore(Doku_Event &$event, $param) {

    $testVars = $this->_testVars();

    // be aware: content and sidebar are handled separately
    // final html-preparation
    $this->_htmlFinalRevise($event->data);

//                    // may be there are widget-ids not unique
//                    $juiInfo = juiGetJuiInfo();
//                    $multiple = $juiInfo->getWidgetListMultiple(juiInfoGetCurPartId());
//                    if ($multiple) {
//                        // handle multiple IDs, make them unique
//                        $html =& $event->data;
//                        $html = $this->_handleMultipleIDs($html, $multiple);
//                    }
    // set part is rendered
    juiInfoSetCurPartRendered();

    $juiInfo = juiGetJuiInfo();
    if ($juiInfo->getIsAllRendered()) {
      // all parts are rendered
      // append variables for JavaScript
      $jsvars = $this->_getJsVarsFinal();
      $event->data .= $this->_getJsVarsHtml($jsvars);
      // request is finished
      return;
    }

    // request is not finished yet
    $juiInfo->resetCurData();       // reset data for sidebar-part
    juiInitAjaxReplace();           // empty replace-array
    juiInfoSetCurPart(TXT_CONTENT); // reset current part to 'content'
  }

  public function handleContentDisplayBefore(Doku_Event &$event, $param) {

    $testVars = $this->_testVars();

    // be aware: content and sidebar are handled separately
    // final html-preparation
    $this->_htmlFinalRevise($event->data);

    // set part is rendered
    juiInfoSetCurPartRendered();
    if (juiGetJuiInfo()->getIsAllRendered()) {
      // all parts are rendered
      // append variables for JavaScript
      $jsvars = $this->_getJsVarsFinal();
      $event->data .= $this->_getJsVarsHtml($jsvars);
      // request is finished
    }
  }

  /**
   * Entweder purge oder Abhängigkeiten setzen
   * 
   * @global type $ID
   * @param Doku_Event $event
   * @param type $param
   * @return type 
   */
  public function handleParserCacheBefore(Doku_Event &$event, $param) {
    global $ID;

    // ensure initializing
    $this->_juiInitialize();

    $data = & $event->data;
    $mode = $data->mode;
//                $pg = $data->page;
//                if ($pg != $ID) {
//                    juiDbgMsg("pg!=ID:: '$pg' != '$ID' - handleParserCacheBefore()", -1);
//                }


    $juiInfo = juiGetJuiInfo();

    $cur_page = & $juiInfo->getCurPage();
    $testVars = $this->_testVars();
    $pid = $cur_page['id'];

    //_getPagePath
    if ($ID == 'sidebar') {
      $xx = 0;        //      Before
    }

    $xx = $cur_page['id'];
    if ($cur_page['id'] != $ID && !$juiInfo->isPageNesting($cur_page, $ID)) {

//                    if ($ID == 'dev:tabs:lout_buch_fs') {
//                        //JUIFIX:: tmp !!!
//                        $data->depends['purge'] = true;
//                    }

      if ($this->isPreview()) {
        $data->depends['purge'] = true;
      }

      // create new page if not exists
      $page = & $juiInfo->getPage($ID, true);
      // add the page to the current page or part and set it to current
      $page = & $juiInfo->addPage($page, $cur_page, true);
      if (!$page) {
        juiDbgMsg("JuiError addPage('$ID') - handleParserCacheBefore()", -1);
        return false;
      }

      if (!$data->depends['purge'] && $juiInfo->ensurePageJuiVars($page)) {
        if ($page['juivars']['purge']) {
          $data->depends['purge'] = true;
        }

        $testVars = $this->_testVars();

        // ajax-call: is purge necessary ?
        if (juiGetIsAjaxCall()) {
          $data->depends['purge'] = $this->_ajaxDoPurge($page);
        }

        // append depends if necessary
        $depends = & $page['juivars']['depends'];
        if (!$data->depends['purge'] && !empty($depends)) {
          $files = & $data->depends['files'];
          foreach ($depends as $file) {
            $file = DOKU_PLUGIN . $file;
            if (in_array($file, $files))
              continue;
            $files[] = $file;
          }
        }
      }
    }
  }

  private function _ajaxDoPurge($page) {
    foreach (($plugins = juiGetJuiPlugins()) as $plugin) {
      if (!juiGetIsNesting($plugin))
        continue; // nothing to do

      if (!empty($page['juivars']['widgetsSubpages'])) {
        // no more analysing subpages but purge
        return true;
      }

      $jsvars = $page['juivars']['jsvars'];
      if (!empty($jsvars)) {
        // $jsvars contains all jui-widgets of that page
        foreach ($jsvars as $id => $vars) {
          if ($vars['plugin'] == $plugin) {
            // nesting equal widget not supported
            return true;
          }
        }
      }
    }

    // no subpages, no nesting widgets: purge is not necessary
    return false;
  }

  public function handleParserCacheAfter(Doku_Event &$event, $param) {
    global $ID;
    static $besucht = array();

    $data = & $event->data;
    $mode = $data->mode;
    $use = $event->result;

    $testVars = $this->_testVars();
    $part = $testVars['curPart']['id'];

    if ($part == 'content' && $ID == 'dev:include01') {
      $xx = 0;        //      After
    }
    $last = $besucht[count($besucht) - 1];
    $besucht[] = $ID;

    if ($data->mode != 'xhtml') {
      return false;
    }

    if ($part == 'content' && $ID == 'dev:acc02') {
      $xx = 0;        //      After
    }
    if ($ID == 'sidebar') {
      $xx = 0;        //      After
    }

    if ($event->result) {
      // cache is used: we have to retrieve needed jui-data
      $juiInfo = juiGetJuiInfo();
      $page = & juiInfoGetCurPage();
      if ($page['id'] == $ID && $juiInfo->ensurePageJuiVars($page)) {
        $juiInfo->includePageWidgets($page);
      }
    }


    $part = juiInfoGetCurPartId();

    $xx = 0;
  }

  public function handleWikipageWrite(Doku_Event &$event, $param) {
    $data = $event->data;
    if (is_array($data) && is_array($data[0]) && count($data[0]) > 1) {
      $event->data[0][1] = $this->_ensureWidgetUID($event->data[0][1]);
    }
  }

  //
  //
  // private functions
  //
  //


  private function _createJuiInfo() {
    global $ID, $ACT;

    $sidebar_hide = $this->_tpl_sidebar_hide($action);

    // determine parts
    $parts = array(TXT_CONTENT);
    if (!juiGetIsAjaxCall()) {
      // not for ajax-call and not for Edit-Preview
      $template = $GLOBALS['conf']['template'];
      // only default and arctic supported
      if ($template == 'arctic' || $template == 'arctic-mbo') {
//                            // include custom arctic template functions
//                            require_once(template('tpl_functions.php'));
        if (!$sidebar_hide) {
          switch (tpl_getConf('sidebar')) {
            case 'left': $parts[] = TXT_SIDEBAR_LEFT;
              break;
            case 'right': $parts[] = TXT_SIDEBAR_RIGHT;
              break;
            case 'both': $parts[] = TXT_SIDEBAR_LEFT;
              $parts[] = TXT_SIDEBAR_RIGHT;
          }
        }
      }
    }

    // create and return juiinfo-object
    return new JuiInfo($parts, $action);
  }

  /**
   * angepasste Kopie von tpl_sidebar_hide()
   * 
   * zu dem frühen Zeitpunkt ist $ACT noch nicht aufbereitet
   * 
   * @global type $ACT
   * @param type $action Rückgabewert
   * @return type boolean
   */
  private function _tpl_sidebar_hide(&$action) {
    global $ACT;

    $action = act_clean($ACT);
    $act_hide = array('edit', 'preview', 'admin', 'conflict', 'draft', 'recover');
    if (in_array($action, $act_hide)) {
      return true;
    } else {
      return false;
    }
  }

  private function _htmlFinalRevise(&$html) {
    $juiInfo = juiGetJuiInfo();

    // may be there are widget-ids not unique
    $multiple = $juiInfo->getWidgetListMultiple(juiInfoGetCurPartId());
    if ($multiple) {
      // handle multiple IDs, make them unique
      $html = $this->_handleMultipleIDs($html, $multiple);
    }

    //
    $replace = juiGetAjaxReplace();
    if ($replace) {
      $html = str_replace(array_values($replace), array_keys($replace), $html);
    }
  }

  private function _handleMultipleIDs($html, $list) {
    if (empty($list))
      return($html);
    foreach ($list as $widget) {
      $html = $this->_handleMultipleID($html, $widget);
    }
    return $html;
  }

  private function _handleMultipleID($html, $id) {
    list($plugin, $foo) = explode('__0__', $id, 2);
    $search = '<div id="' . $id . '"';
    $parts = explode($search, $html);
    if (count($parts) < 2) {
      return $html;
    }

    $ret = array_shift($parts);
    foreach ($parts as $part) {
      $replace = '__' . JuiCounter::uidNext() . '__';
      // prepend the new id-string
      $ret .= str_replace('__0__', $replace, $search);
      //
      $s = "$id";
      $r = str_replace('__0__', $replace, $s);

      $ret .= str_replace($s, $r, $part);
    }

    return $ret;
  }

  private function _getJsVarsFinal() {
//        global $conf;
    if (!juiExistsJuiPlugins()) {
      return false;
    }

//                    $testVars = $this->_testVars();



    $data = array();
    $widgetsExists = array();
    $widgetsIDs = array();
    $widgets = array();
    $pluginsDefaults = array();

    $juiDataIO = JuiDataIO::getInstance();
    $juiInfo = juiGetJuiInfo();
    $pages = & $juiInfo->getPages();
    foreach ($pages as $page) {
      $juivars = $page['juivars'];
      if (empty($juivars))
        continue;  // doesn't contain jui-widgets

// widgetsExists
      if (is_array($juivars['widgetsExists'])) {
        foreach ($juivars['widgetsExists'] as $plugin => $foo) {
          $widgetsExists[$plugin] = true;
        }
      }

      // widgets/$widgetsIDs - $juivars['jsvars'] contains only widgets
      if (is_array($juivars['jsvars'])) {
        foreach ($juivars['jsvars'] as $juiid => $jsvars) {
          $cid = juiCleanID($juiid);      // remove '__0__'
          if (in_array($cid, $widgetsIDs))
            continue;    // already done

          $widgetsIDs[] = $cid;
          $widgets[$cid] = $jsvars;
          $state = $juiDataIO->retrieveData('state', $cid);
          $state = json_decode($state, true);
          if (empty($state))
            $state = array('foo' => ''); // not empty
          $widgets[$cid]['state'] = $state;
        }
      }
    }

    // data of all default-schemes
    $schemes = & JuiSchemes::getInstance();
    foreach (($plugins = juiGetJuiPlugins()) as $plugin) {
      $scheme = $schemes->getSchemeInstance($plugin);    // default
      $pluginsDefaults[$plugin] = $scheme->getSchemeData();
    }

    // parts: left_sidebar=1, right_sidebar=2, none=0, both=3
    $parts = $juiInfo->getParts();
    $p_cnt = count($parts);
    $pgparts = 0;
    if ($p_cnt == 2) {
      $pgparts += isset($parts[TXT_SIDEBAR_LEFT]) ? 1 : 2;
    } elseif ($p_cnt == 3) {
      $pgparts = 3;
    }

    $cur = array('isPreview' => $this->isPreview() ? 1 : 0);
    $conf = array('juitoggler' => $this->_getJsTogglerVars(),
        'hidepagename' => $this->getConf('hide_pagename'),
        'insertpageicon' => $this->getConf('insert_pageicon')
    );
    // page-state
    global $ID;
    $pgstate = $juiDataIO->retrieveData('state', $ID);
    $pgstate = json_decode($pgstate, true);
    if (empty($pgstate))
      $pgstate = array('foo' => ''); // not empty

    // user-state
    global $ID;
    $usrstate = $juiDataIO->retrieveData('state', '@user@');
    $usrstate = json_decode($usrstate, true);
    if (empty($usrstate))
      $usrstate = array('foo' => ''); // not empty

    $ret = array('widgets' => $widgets,
        'ajaxData' => juiGetAjaxData(),
        'window' => array('widgetsExists' => $widgetsExists,
            'widgetsIDs' => $widgetsIDs,
            'pluginsDefaults' => $pluginsDefaults,
            'pgparts' => $pgparts,
            'cur' => $cur,
            'conf' => $conf,
            'pgstate' => $pgstate,
            'usrstate' => $usrstate
        )
    );

//                        // return false if there is no jui-widget
//                        if ( empty($ret['window']['widgetsIDs']) ) {
//                            return false;
//                        }
    // remove empty Arrays (deep)
    juiRemoveEmptyArrays($ret, true);

    // append the last used number of jui-widget-uids
    $ret['window']['uidLast'] = JuiCounter::uidLast();
    $ret['window']['juiPlugins'] = juiGetJuiPlugins();
    return $ret;
  }

  private function _getJsTogglerVars() {
    if (!$_SERVER['REMOTE_USER'] || !$this->getConf('juitoggler')) {
      return array('enabled' => false);
    }

    return array('enabled' => true,
        'pinable' => $this->getConf('juitoggler_pinable'),
        'lockable' => $this->getConf('juitoggler_lockable')
    );
  }

  private function _getJsVarsHtml($jsvars) {
    if (!isset($jsvars))
      $jsvars = array();
    return DOKU_LF . '<script type="text/javascript" charset="utf-8"><!--//--><![CDATA[//><!--'
            . DOKU_LF . "juivars=" . json_encode($jsvars) . ';'
            . DOKU_LF . '//--><!]]></script>' . DOKU_LF;
  }

  /**
   * Ensures for each $jui-widget (juiacc, juitabs, ...) in $content
   * that there is an uid-attribute
   *
   * Best practice for user: leave uid empty, will get an uid automatically
   *
   * @param string $content
   * @return string $content with each tag having uid-attribute
   */
  private function _ensureWidgetUID($content) {
    $changes = array();
    //JUI-FIX::  *** $jui nicht belegt - _ensureWidgetUID
    foreach (($plugins = juiGetJuiPlugins()) as $plugin) {
      $pattern = str_replace('{JUI}', $plugin, '&<{JUI}.+?</{JUI}>&is');
      preg_match_all($pattern, $content, $matches);
      foreach ($matches[0] as $match) {
        list($search, $foo) = explode('>', $match, 2);
        if (empty($foo)) {
          continue;
        }

        $search .= '>';
        $opts = trim(substr($search, strlen($plugin) + 1, -1));

        $add = false;
        if (empty($opts)) {
          $add = true;
        } else {
          $a = juiGetOptionsArray($opts, array('uid' => '*', 'oid' => '*'));
          if (!isset($a['uid'])) {
            $add = true;
          }
          $testuid = $a['uid'];
          $testoid = $a['oid'];
        }

        if ($add) {
          list($pre, $post) = explode(' ', $search, 2);
          if (empty($post)) {
            $pre = substr($pre, 0, -1);
            $post .= '>';
          }
          $p = array();
          $p['uid'] = time() + JuiCounter::uidNext(); // one second is a too long time
          $att = ' ' . buildAttributes($p) . ' ';
          $replace = $pre . $att . $post;
          $changes[] = array("search" => $search, "replace" => $replace);
          $testuid = $p['uid'];
        }

        // restore state if needed
        if (isset($testuid) && isset($testoid)) {
          $juiDataIO = JuiDataIO::getInstance();
          $target = 'state';
          $keyuid = $plugin . $testuid;
          $data = $juiDataIO->retrieveData($target, $keyuid);
          if (empty($data)) {
            $testoid = preg_split("/[\s,;]+/", $testoid);
            foreach ($testoid as $oid) {
              $data = $juiDataIO->retrieveData($target, $plugin . $oid);
              if (!empty($data)) {
                $juiDataIO->storeData($target, $keyuid, $data);
                break;
              }
            }
          }
        }
      }
    }

    // insert required uid for each
    foreach ($changes as $change) {
      $content = str_replace($change['search'], $change['replace'], $content);
    }

    return $content;
  }

  private function _getPageTags($raw_id) {
    return array("<!-- PageTag $raw_id -->",
        "<!-- PageTag /$raw_id -->");
  }

  //
  // Testfunctions
  //
  private function _testVars() {
    global $ID;
    $ret = array();

    $ret['ID'] = $ID;
    $juiInfo = juiGetJuiInfo();
    $ret['juiInfoParts'] = & $juiInfo->getParts();
    $ret['juiInfoPages'] = & $juiInfo->getPages();
    $ret['juiInfoWidgets'] = & $juiInfo->getWidgets();
    $ret['curPart'] = & $juiInfo->getCurPart();
    $ret['curPage'] = & $juiInfo->getCurPage();
    $ret['getCurWidget'] = & $juiInfo->getCurWidget();

    return $ret;
  }

  private function _test(&$event, $param) {
    //$event->data[] = 'alert(\'Test successful: juihelper\');';
    $xx = 0;
    return;
  }

}

// vim:ts=4:sw=4:et:
