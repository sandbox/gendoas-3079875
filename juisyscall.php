<?php
if (!defined('DOKU_INC')) die();
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

/**
 * Description of syspage
 *
 * @author Albin
 */
class juisyscall {
    private $_html_output = '';
    private $_html_error = '';
    
    public function getHtmlOutput() {
        return $this->_html_output;
    }
    public function getHtmlError() {
        return '<div class="error">'.$this->_html_error.'</div>';
    }
    
    public function parse($call, $data) {
        if (!$this->_isSyscallAllowed()) {
            global $lang;
            $this->_html_error = $lang['accessdenied'];
            return false;
        }
        
        if (!is_string($data)) {
            $this->_html_error = 'Not supported.';
            return false;
        }

        $data = ltrim($data, ":");
        list($sys, $data) = explode('-', $data, 2);
        if ($sys != '@sys@') {
            $this->_html_error = 'Not supported.';
            return false;
        }
        
        
        $callfn = '_parse_' . strtolower(trim($call));
        if (method_exists($this, $callfn)) {
            return $this->$callfn($data);
        }
        $this->_html_error = 'Not found: juisyscall->'.$callfn;
        return false;
    }
    private function _isSyscallAllowed() {
        $allowed = array('dialog');
        $extra = $GLOBALS['juiINFO']['syscall']['extra'];
        $caller = isset($extra) && isset($extra['caller']) ? $extra['caller'] : '';
//        return isset($caller) && $caller == 'dialog' ? true : false;
        return isset($caller) && in_array($caller, $allowed) ? true : false;
    }
    
    private function _parse_page($syspage) {
      global $ID;
        $err = "juisyscall._parse_page({$syspage})";
        list($plugin, $page) = explode('-', $syspage, 2);
        $page = str_replace('-', '/', $page);
        $text = @file_get_contents(DOKU_PLUGIN."{$plugin}/syspages/{$page}.txt");
        if ($text) {
            // replace macros
            $text = $this->_applyMacro($text);
            // prepare some things for juivars
            $keep_id = $ID;
            $ID = $syspage;
            $juiInfo = juiGetJuiInfo();
            $juiInfo->setIsSyspage();
            $juiInfo->setCurPart(TXT_CONTENT);
            $page =& $juiInfo->getPage($syspage, true);
            $page =& $juiInfo->addPage($page, $juiInfo->getCurPart(), true);
                    //LANG:: siehe include/helper.php - _apply_macro()
                    //return str_replace(array_keys($replace), array_values($replace), $id);
            // render now
            $this->_html_output = p_render('xhtml', p_get_instructions($text), $info);
            $ID = $keep_id;
            return true;
        }
        
        $this->_html_error = $err;
        return false;
    }
    
    private function _parse_form($sysform) {
        $err = "juisyscall._parse_form({$sysform})";
        list($plugin, $form) = explode('-', $sysform, 2);
        $form = str_replace('-', '/', $form);
        list($file, $method) = explode('.', $form, 2);
        $class = $plugin . '_' . str_replace('/', '_', $file);
        $method = 'html_' . $method;
        $file = DOKU_PLUGIN."{$plugin}/syspages/{$file}.php";
       
        if (!@file_exists($file)) {
            list($foo, $file) = explode(DOKU_PLUGIN, $file, 2);
            $this->_html_error = $err . "\n<br />File not found: $file";
            return false;
        };
        
        require_once $file;
        if (!class_exists($class)) {
            $this->_html_error = $err;
            return false;
        }
        
        $cls = new $class();
        $this->_html_output = $cls->$method();
        return true;
    }
    
    private function _parse_locale($syslocale) {
        $err = "juisyscall._parse_locale({$syslocale})";
        list($plugin, $page) = explode('-', $syslocale, 2);
        $text = juiGetRawLacale($page, $plugin);
        if (empty($text)) {
            $this->_html_error = $err;
            return false;
        }
        
        $this->_html_output = $text;
        return true;
    }
    
    private function _applyMacro($text) {
        $replace = array(
                '@PAGES@'  => juiGetLang('pages'),
                );
        return str_replace(array_keys($replace), array_values($replace), $text);
    }
}

?>
