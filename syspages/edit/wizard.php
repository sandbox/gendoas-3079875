<?php
if (!defined('DOKU_INC')) die('');
if(!defined('NL')) define('NL',"\n");

abstract class jui_edit_wizard {
    
    abstract function getData($target);


    
    
    public function htmlRadio($target, $maxitems=7) {
        $data = $this->getData($target);
        if (empty($data) || !is_array($data)) {
            return $this->getHtmlError();
        }
        
        $items = array();
        $more  = array();
        $cnt = 0;
        if (count($data) > $maxitems) $maxitems--;
        if (count($data) == $maxitems + 2) $maxitems--;
        foreach ($data as $item) {
            if ($cnt < $maxitems) {
                $items[$item] = $item;
            } else $more[$item] = $item;
            $cnt++;
        }
        
        $html_out = '<div class="leftalign juiradio">'.NL;
        $form = new Doku_Form(array('id' => 'juiacc_'.$target));
        $form->startFieldset(ucfirst($target));
        $form->addRadioSet($target, $items);
        if(!empty($more)) {
            $items = array('more' => juiGetLang('more'));
            $form->addRadioSet($target, $items);
            $form->addElement(form_makeListboxField(
                    $target . '_more',
                    $more, '', '', '', '',
                    array('class'=>'quickselect')));
            $form->endFieldset();
        } else $form->endFieldset();

        $html_out .= $this->htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    function htmlCheck($target, $maxitems=8) {
        $data = $this->getData($target);
        if (empty($data)) {
            return $this->getHtmlError();
        }
        
        // prepare data
        $items = array();
        $more  = array();
        $cnt = 0;
        foreach ($data as $item) {
            if ($cnt < $maxitems) {
                $items[$item] = $item;
            } else $more[$item] = $item;
            $cnt++;
            if ($cnt >= 2 * $maxitems) {
                break;
            }
        }
        
        
        $html_out = '<div class="leftalign juicheck">'.NL;
        $form = new Doku_Form(array('id' => 'juiacc_'.$target));
        
        $class = 'simple';
        $tstyle = empty($more) ? "width:100%" : "width:50%";
        // open table
        $form->addElement(form_makeOpenTag('div', array('class'=>'juitable')));
        $form->addElement(form_makeOpenTag('div', array('class'=>'juitr')));
        
        // left col
        $form->addElement(form_makeOpenTag('div', array('class'=>'juitd', 'style'=>$tstyle)));
        $idx = 0;
        foreach ($items as $item) {
            $idx++;
            $form->addElement(form_makeCheckboxField($item, $idx, $item, $item.$idx, $class));
        }
        $form->addElement(form_makeCloseTag('div'));

        // right col, if needed
        if (!empty($more)) {
            $form->addElement(form_makeOpenTag('div', array('class'=>'juitd')));
            foreach ($more as $item) {
                $idx++;
                $form->addElement(form_makeCheckboxField($item, $idx, $item, $item.$idx, $class));
            }
           $form->addElement(form_makeCloseTag('div'));
        }

        // close table
        $form->addElement(form_makeCloseTag('div'));
        $form->addElement(form_makeCloseTag('div'));
        
        // create form-output
        $html_out .= $this->htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    
    
    
    
    public function htmlForm(&$form) {
        // Safety check in case the caller forgets.
        $form->endFieldset();
        return $form->getForm();
    }
    public function getHtmlError($msg='No data found.') {
        return '<div class="error">'.$msg.'</div>';
    }

    
}
?>
